import {STORAGE_NAME} from 'utils/constant';
import api from 'api'
import {config} from 'utils';
import router from 'umi/router';

const {
  userMe, getCountries, getRegions, filterCountryByRegion, uploadFile, getCategories, getProducts,
  filterProductByCategory, getBrands, filterBrandByProduct, getModels, getDashboards, getModelsOpen,
  filterModelByProduct, getOrderByUser, getOrderByAdmin, findModelByName, filterModelByBrand
} = api;

function pagination(page, totalPages) {
  let res = [];
  let from = 1;
  let to = totalPages;
  if (totalPages > 10) {
    from = Math.max(page - 2, 1);
    to = Math.max(Math.min(page + 2, totalPages), 5);
    if (to > 5) {
      res.push(1);
      if (from > 2) res.push(2);
      if (from > 3) {
        if (from === 4) {
          res.push(3);
        } else {
          res.push("...");
        }
      }
    }
  }
  for (let i = from; i <= to; i++) {
    res.push(i);
  }

  if (totalPages > 10) {
    if (to < (totalPages - 2)) {
      if (to === 8) {
        res.push(9);
      } else {
        res.push("...");
      }
    }
    if (to < totalPages)
      res.push(totalPages - 1);
    if (to !== totalPages)
      res.push(totalPages);
  }
  return res;
}


export default {
  namespace: 'app',
  state: {
    page: 0,
    size: 5,
    totalPages: 0,
    paginations: [],
    currentUser: {},
    currentOrder: {},
    countries: [],
    regions: [],
    logoId: '',
    photosId: [],
    categories: [],
    products: [],
    brands: [],
    models: [],
    dashboards: [],
    photoId: {},
    modelsOpen: [],
    productId: '',
    orders: [],
  },
  subscriptions: {
    setupHistory({dispatch, history}) {
      history.listen((location) => {
        dispatch({
          type: 'updateState',
          payload: {
            pathname: location.pathname,
          },
        });
        // dispatch({
        //   type: 'userMe',
        //   payload: {
        //     pathname: location.pathname
        //   }
        // })
      })
    },
    setup({dispatch, history}) {
      history.listen(({pathname}) => {
        if (!config.openPages.includes(pathname)) {
          dispatch({
            type: 'userMe',
            payload: {
              pathname
            }
          })
        }
      })
    }
  },
  effects: {
    * userMe({payload}, {call, put, select}) {
      try {
        const res = yield call(userMe);

        if (!res.success) {
          yield put({
            type: 'updateState',
            payload: {
              currentUser: {}
            }
          });
          localStorage.removeItem(STORAGE_NAME);
          router.push('/auth/login');
          if (payload) {
            if (!config.openPages.includes(payload.pathname)) {
              localStorage.removeItem(STORAGE_NAME);
              router.push('/auth/login')
            }
          } else {
            localStorage.removeItem(STORAGE_NAME);
            router.push('/auth/login')
          }
        } else {
          yield put({
            type: 'updateState',
            payload: {currentUser: res.object}
          })
        }
      } catch (error) {
        yield put({
          type: 'updateState',
          payload: {currentUser: {}}
        });
        localStorage.removeItem(STORAGE_NAME);
        router.push('/auth/login');
        if (payload) {
          if (!config.openPages.includes(payload.pathname) && !payload.pathname.includes('/article/') && !payload.pathname.includes('/videos/')) {
            localStorage.removeItem(STORAGE_NAME);
            router.push('/auth/login')
          }
        } else {
          localStorage.removeItem(STORAGE_NAME);
          router.push('/auth/login')
        }
      }
    },
    * getCountries({payload}, {call, put, select}) {
      const res = yield call(getCountries);
      yield put({
        type: 'updateState',
        payload: {
          countries: res._embedded.countries,
          regions: []
        }
      })
    },

    * getDashboards({payload}, {call, put, select}) {
      const res = yield call(getDashboards);
      yield put({
        type: 'updateState',
        payload: {
          dashboards: res.object
        }
      })
    },
    * filterCountryByRegion({payload}, {call, put, select}) {
      const res = yield call(filterCountryByRegion, payload);
      if (res.success) {
        yield put({
          type: 'updateState',
          payload: {
            regions: res._embedded.regions
          }
        })
      }
    },
    * getRegions({payload}, {call, put, select}) {
      const res = yield call(getRegions);
      if (res.success) {
        yield put({
          type: 'updateState',
          payload: {
            regions: res._embedded.regions
          }
        })
      }
    },

    * uploadPhoto({payload}, {call, put, select}) {
      return yield call(uploadFile, {payload});
    },

    * getOrder({payload}, {call, put, select}) {
      const {page, size, currentUser} = yield select(_ => _.app);
      if (!payload) {
        payload = {page, size};
      }
      const res = yield call(currentUser.authorities.length > 1 ? getOrderByAdmin : getOrderByUser, payload);
      if (res.success) {
        yield put({
          type: 'updateState',
          payload: {
            orders: res.object,
            page: res.currentPage,
            totalPages: res.totalPages,
            paginations: pagination(res.currentPage, res.totalPages)
          }
        })
      }
    },
    * getCategories({payload}, {call, put, select}) {
      const res = yield call(getCategories);
      if (res.success) {
        yield put({
          type: 'updateState',
          payload: {
            categories: res._embedded.categories
          }
        })
      }
    },
    * getProducts({payload}, {call, put, select}) {
      const res = yield call(getProducts);
      if (res.success) {
        yield put({
          type: 'updateState',
          payload: {
            products: res._embedded.products
          }
        })
      }
    },
    * filterProductByCategory({payload}, {call, put, select}) {
      const res = yield call(filterProductByCategory, payload);
      if (res.success) {
        yield put({
          type: 'updateState',
          payload: {
            products: res._embedded.products
          }
        })
      }
    },
    * getBrands({payload}, {call, put, select}) {
      const res = yield call(getBrands);
      if (res.success) {
        yield  put({
          type: 'updateState',
          payload: {
            brands: res._embedded.brands
          }
        })
      }
    },
    * filterBrandByProduct({payload}, {call, put, select}) {
      const res = yield call(filterBrandByProduct, payload);
      if (res.success) {
        yield put({
          type: 'updateState',
          payload: {
            brands: res._embedded.brands
          }
        })
      }
    },

    * getModels({payload}, {call, put, select}) {
      const {page, size} = yield select(_ => _.app);
      if (!payload) {
        payload = {page, size};
      }
      const res = yield call(getModels, payload);
      if (res.success) {

        yield put({
          type: 'updateState',
          payload: {
            models: res.object,
            page: res.currentPage,
            totalPages: res.totalPages,
            paginations: pagination(res.currentPage, res.totalPages)
          }
        })
      }
    },
    * getModelsOpen({payload}, {call, put, select}) {
      const res = yield call(getModelsOpen);
      if (res.success) {
        yield put({
          type: 'updateState',
          payload: {
            modelsOpen: res.object
          }
        })
      }
    },
    * filterModelByProduct({payload}, {call, put, select}) {
      const res = yield call(filterModelByProduct, payload);
      if (res.success) {
        yield put({
          type: 'updateState',
          payload: {
            modelsOpen: res.list
          }
        })
      }
    },
    * filterModelByBrand({payload}, {call, put, select}) {
      const res = yield call(filterModelByBrand, payload);
      if (res.success) {
        yield put({
          type: 'updateState',
          payload: {
            modelsOpen: res.list
          }
        })
      }
    },
    * findModelByName({payload}, {call, put, select}) {
      const res = yield call(findModelByName, payload);
      if (res.success) {
        yield put({
          type: 'updateState',
          payload: {
            modelsOpen: res.list
          }
        })
      }

    }


  },
  reducers: {
    updateState(state, {payload}) {
      return {
        ...state,
        ...payload
      }
    }
  }

}
