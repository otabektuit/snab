export default {

  signUp: 'POST /auth/register',
  signIn: 'POST /auth/login',
  userMe: '/auth/me',

  uploadFile: 'POST /attach/upload',

  getCountries: '/country',
  getCountry: '/country',
  addCountry: 'POST /country',
  editCountry: 'PUT /country',
  deleteCountry: 'DELETE /country',

  getRegions: '/region',
  getRegion: '/region',
  addRegion: 'POST /region',
  editRegion: 'PUT /region',
  deleteRegion: 'DELETE /region',
  filterCountryByRegion: '/region/search/filterByCountry',

  getCategories: '/category',
  getCategory: '/category',
  addCategory: 'POST /category',
  editCategory: 'PUT /category',
  deleteCategory: 'DELETE /category',

  getProducts: '/product',
  getProduct: '/product',
  addProduct: 'POST /product',
  editProduct: 'PUT /product',
  deleteProduct: 'DELETE /product',
  filterProductByCategory: '/product/search/filterByCategory',

  getBrands: '/brand',
  getBrand: '/brand',
  addBrand: 'POST /brand',
  editBrand: 'PUT /brand',
  deleteBrand: 'DELETE /brand',
  filterBrandByProduct: '/brand/search/filterByProduct',

  getModels: '/model',
  getModelsOpen: '/model/list',
  getModel: '/model',
  addModel: 'POST /model',
  editModel: 'PUT /model',
  deleteModel: 'DELETE /model',
  findModelByName: 'GET /model/findModelByName',
  filterModelByProduct: '/model/filterByProduct',
  filterModelByBrand: '/model/filterByBrand',

  getUsers: '/user',
  getUser: '/user',
  deleteUser: 'DELETE /user',
  editUser:'PATCH /user/edit',

  getDashboards: '/dashboard',
  getDashboard: '/dashboard',
  addDashboard: 'POST /dashboard',
  editDashboard: 'PUT /dashboard',
  deleteDashboard: 'DELETE /dashboard',


  addOrder: 'POST /order/add',
  getOrderByUser: '/order/getOrderByUser',
  getOrderByAdmin: '/order',
  deleteOrder: 'DELETE /order/deleteOrder',

}

