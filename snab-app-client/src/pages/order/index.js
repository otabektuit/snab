import React, {Component} from 'react';
import {connect} from "react-redux";
import SideBarAdmin from "../../component/SideBarAdmin";
import {Col, Modal, ModalHeader, Pagination, PaginationItem, PaginationLink, Row} from "reactstrap";
import {toast} from "react-toastify";
import router from "umi/router";
import {FormattedMessage} from "umi-plugin-locale";

let tempSize = 0;

@connect(({order, app}) => ({order, app}))
class Order extends Component {
  componentDidMount() {
    const {dispatch} = this.props;
    dispatch({
      type: 'app/getOrder'
    })
  }

  render() {
    const {dispatch, order, app} = this.props;
    const {page, paginations, size, orders,current} = app;
    const {totalPages, deleteModal, orderId} = order;

    const selectSize = (e) => {
      tempSize = e.target.value;
    };

    const getOrder = (e, page) => {
        dispatch({
          type: 'app/getOrder',
          payload: {
            page: tempSize > 0 ? 0 : page,
            size: tempSize > 0 ? tempSize : size
          }
        })
    };

    const openModalDelete = (id) => {
      dispatch({
        type: 'order/updateState',
        payload: {
          deleteModal: !deleteModal,
        }
      })
      dispatch({
        type: 'order/updateState',
        payload: {
          orderId: id
        }
      })
    };
    const deleteOrder = () => {
      dispatch({
        type: 'order/deleteOrder',
        payload: {
          id: orderId
        }
      }).then(res=>{
        if (res.success) {
          dispatch({
            type: 'order/updateState',
            payload: {
              deleteModal: false,
              deletedId: ''
            }
          });
          toast.success("Заказ отменено");
          dispatch({
            type:'app/getOrder'
          });
        } else {
          toast.error("Ошибка в отказе");
        }
      }
      )
    };

    const orderInfo=(item)=>{
      dispatch({
        type:'app/getModel',
        payload:{
          path:item.modelId
        }
      });
      router.push('/order/add')
    };

    return (
      <div>
        {

        }
        <div className="container">
          <div className="row">
            <div className="col mt-5">
              <h1 className="my-4 text-cente">
                <FormattedMessage id="USERORDERS"/>
              </h1>
              <div className="row">
                <div className="col-8">

                  <a className="btn btn-success" href="/products"><FormattedMessage id="ORDER"/></a>
                </div>

                <div className="col-1 text-right mr-5">
                  <select className="px-3 py-2 border-primary rounded" onChange={selectSize}>
                    <option value="5">5</option>
                    <option value="8">8</option>
                    <option value="10">10</option>
                  </select>
                </div>
                <div className="col-1">
                  <button className="btn btn-dark" onClick={(e) => getOrder(e, page)}>
                    <FormattedMessage id="FILTER"/>
                  </button>
                </div>
              </div>

              {orders.length === 0 ? <h1><FormattedMessage id="NOORDER"/></h1> :
                <table className="table text-center mt-2">
                  <thead className="thead-light">
                  <tr>
                    <th>#</th>
                    <th>ModelName</th>
                    <th>Brand name</th>
                    <th>Product name</th>
                    <th>Option</th>
                    <th>Action</th>
                  </tr>
                  </thead>
                  <tbody>
                  {orders.map((item, i) =>
                    <tr key={item.id} >
                      <td onClick={()=>orderInfo(item)}>{i + 1}</td>
                      <td>{item.modelName}</td>
                      <td>{item.brandName}</td>
                      <td>{item.productNameUz}</td>
                      <td>{item.price}</td>
                      <td>
                        <button className="btn btn-danger" onClick={() => openModalDelete(item.id)}><FormattedMessage id="DELETE"/></button>

                      </td>
                    </tr>
                  )}
                  </tbody>
                </table>
              }
            </div>
          </div>
          <Row className="mt-5 mb-5">
            <Col md={2} className="offset-5">
              <Pagination aria-label="Page navigation example">
                <PaginationItem disabled={page === 0}>
                  <PaginationLink first onClick={(e) => getOrder(e, 0)}/>
                </PaginationItem>
                <PaginationItem disabled={page === 0}>
                  <PaginationLink previous onClick={(e) => getOrder(e, page - 1)}/>
                </PaginationItem>
                {paginations.map(i =>
                  <PaginationItem key={i} active={page === (i - 1)}
                                  disabled={page === (i - 1) || i === '...'}>
                    <PaginationLink onClick={(e) => getOrder(e, (i - 1))}>
                      {i}
                    </PaginationLink>
                  </PaginationItem>)}
                <PaginationItem disabled={(totalPages - 1) === page}>
                  <PaginationLink next onClick={(e) => getOrder(e, page + 1)}/>
                </PaginationItem>
                <PaginationItem disabled={page === (totalPages - 1)}>
                  <PaginationLink last onClick={(e) => getOrder(e, (totalPages - 1))}/>
                </PaginationItem>
              </Pagination>
            </Col>
          </Row>
          <div className="row">
            <div className="col">
              <Modal isOpen={deleteModal} toggle={openModalDelete}>
                <ModalHeader>
                  <h2>Delete order</h2>
                </ModalHeader>
                <button onClick={deleteOrder}>Удалить</button>
                <button onClick={openModalDelete}>Отменить</button>
              </Modal>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

Order.propTypes = {};

export default Order;
