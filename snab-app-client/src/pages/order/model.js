import api from 'api'
import {toast} from "react-toastify";



const {addOrder,deleteOrder} = api;
export default {
  namespace: 'order',
  state: {
    page: 0,
    size: 1,
    totalPages: 0,

    deleteModal: false,
    deletedId: '',
    orderId:''
  },
  subscriptions: {},
  effects: {
    * addOrder({payload}, {call, put, select}) {
      const res = yield call(addOrder, payload);
      if (res.success) {
        toast.success("Ваш заказ принят");
      } else {
        toast.error("Ваш заказ не принят");
      }
    },

    * deleteOrder({payload}, {call, put, select}) {
      const res = yield call(deleteOrder, payload);
      return res;
    }
  },
  reducers: {
    updateState(state, {payload}) {
      return {
        ...state,
        ...payload
      }
    }
  }
}
