import React, {Component} from 'react';
import OrderInfoForm from "../../../component/OrderInfoForm";
import {connect} from "react-redux";
import router from "umi/router";


@connect(({app, order}) => ({app, order}))
class AddOrder extends Component {
  componentDidMount() {
    const {app} = this.props;
    const {currentOrder} = app;
    let savedState = JSON.stringify(currentOrder);
    if (savedState!=="{}") {
      console.log(savedState)
      localStorage.setItem('modelState', savedState);
    }
  }

  render() {
    const {app, dispatch} = this.props;
    const {currentOrder} = app;
    const add = () => {
      dispatch({
        type: "order/addOrder",
        payload: {
          modelId: currentOrder.id
        }
      });
      router.push("/order")
    };
    let retrievedState = JSON.parse(localStorage.getItem('modelState'));
    return (
      <div>
        <OrderInfoForm add={add} currentOrder={currentOrder.photosId ? currentOrder : retrievedState}/>
      </div>
    );
  }
}

AddOrder.propTypes = {};

export default AddOrder;
