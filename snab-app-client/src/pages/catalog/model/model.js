import api from 'api'
import {toast} from "react-toastify";

const {getModel, addModel, editModel, deleteModel, filterCountryByModel, getModels} = api;

export default {
  namespace: 'model',
  state: {
    currentModel: {},
    deleteModal: false,
    deletedId: '',
    modalniOchish:false,
  },
  subscriptions: {},
  effects: {
    * addModel({payload}, {call, put, select}) {
      const res = yield call(addModel, payload);
      if (res.success) {
        yield put({
          type: 'updateState',
          payload: {
            isShowModal: false
          }
        });
        yield put({
          type: 'app/getModels'
        });
        toast.info(res.message);
      } else {
        toast.error(res.message);
      }
    },
    * editModel({payload}, {call, put, select}) {
      const res = yield call(editModel, payload);
      if (res.success) {
        yield put({
          type: 'updateState',
          payload: {
            modalniOchish: false,
            currentModel: {}
          }
        });
        yield put({
          type: 'app/getModels'
        });
        toast.success(res.message);
      } else {
        toast.error(res.message);
      }
    },
    * deleteModel({payload}, {call, put, select}) {
      const res = yield call(deleteModel, payload);
      if (res.success) {
        yield put({
          type: 'updateState',
          payload: {
            deleteModal: false,
            deletedId: ''
          }
        });
        yield put({
          type: 'app/getModels'
        });
        toast.success(res.message);
      } else {
        toast.error(res.message);
      }
    }

  },
  reducers: {
    updateState(state, {payload}) {
      return {
        ...state,
        ...payload
      }
    }
  }
}
