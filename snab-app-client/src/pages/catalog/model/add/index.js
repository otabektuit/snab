import React, {Component} from 'react';
import {connect} from "react-redux";
import ModelForm from "../../../../component/ModelForm";
import {router} from "umi";

@connect(({app, model}) => ({app, model}))
class AddModel extends Component {
  componentDidMount() {
    const {dispatch} = this.props;
    dispatch({
      type: 'app/getModels'
    });
    dispatch({
      type: 'app/getBrands'
    });
    dispatch({
      type: 'app/getProducts'
    });
  }

  render() {
    const {app, model, dispatch} = this.props;
    const {photosId, products, brands} = app;
    const {currentModel} = model;

    const add = (e, v) => {
        dispatch({
          type: 'model/addModel',
          payload: {
            ...v,
            photosId
          }
        })

      router.push('/catalog/model')
    };


    const uploadPhotos = (e) => {
      if (e.target.files[0]) {
        dispatch({
          type: 'app/uploadPhoto',
          payload: {
            fileUpload: true,
            file: e.target.files,
          }
        }).then(res => {
            if (res.success) {
              let photos = [...photosId];
              photos.push(res.object);
              dispatch({
                type: 'app/updateState',
                payload: {
                  photosId: photos
                }
              })
            }
          }
        )
      }
    };

    return (
      <div>
        <ModelForm products={products} uploadPhotos={uploadPhotos}
                     add={add} brands={brands} model={currentModel}
        />
      </div>
    );
  }
}

AddModel.propTypes = {};

export default AddModel;
