import React, {Component} from 'react';
import {connect} from "react-redux";
import ModelForm from "../../../../component/ModelForm";
import {router} from "umi";

@connect(({app, model}) => ({app, model}))
class EditModel extends Component {
  componentDidMount() {
    const {dispatch} = this.props;
    dispatch({
      type: 'app/getModels'
    });
    dispatch({
      type: 'app/getBrands'
    });
    dispatch({
      type: 'app/getProducts'
    });
  }

  render() {
    const {app, model, dispatch} = this.props;
    const {photosId, products, brands} = app;
    const {currentModel} = model;

    const edit = (e, v) => {
        dispatch({
          type: 'model/editModel',
          payload: {
            path: currentModel.id,
            ...v,
            photosId
          }
        })
      router.push('/catalog/model')
    };

    const uploadPhotos = (e) => {
      if (e.target.files[0]) {
        dispatch({
          type: 'app/uploadPhoto',
          payload: {
            fileUpload: true,
            file: e.target.files,
          }
        }).then(res => {
            if (res.success) {
              let photos = currentModel.id==null ? [...currentModel.photosId] : [...photosId];
              photos.push(res.object);
              dispatch({
                type: 'app/updateState',
                payload: {
                  photosId: photos
                }
              })
            }
          }
        )
      }
    };

    return (
      <div>
        <ModelForm products={products} uploadPhotos={uploadPhotos}
                   edit={edit} brands={brands} model={currentModel}
        />
      </div>
    );
  }
}

EditModel
  .propTypes = {};

export default EditModel;
