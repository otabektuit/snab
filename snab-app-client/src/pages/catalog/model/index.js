import React, {Component} from 'react';
import router from "umi/router";
import {connect} from "react-redux";
import {Col, Modal, ModalHeader, Pagination, PaginationItem, PaginationLink, Row} from "reactstrap";
import {STORAGE_NAME} from 'utils/constant';
import SideBarAdmin from "../../../component/SideBarAdmin";

let tempSize = 0;

@connect(({model, app}) => ({model, app}))
class Model extends Component {
  componentDidMount() {
    const {dispatch} = this.props;
    dispatch({
      type: 'app/getModels'
    })
  }

  render() {
    const {dispatch, model, app} = this.props;
    const {models, page, paginations, size,} = app;
    const {totalPages, currentModel, deleteModal} = model;

    const addModel = () => {
      dispatch({
        type: 'model/updateState',
        payload: {
          currentModel: {}
        }
      })
      router.push("model/add")
    };
    const token = localStorage.getItem(STORAGE_NAME);
    const selectSize = (e) => {
      tempSize = e.target.value;
    };

    const getModels = (e, page) => {
      dispatch({
        type: 'app/getModels',
        payload: {
          page: tempSize > 0 ? 0 : page,
          size: tempSize > 0 ? tempSize : size
        }
      })
    };

    const getModel = (item) => {
      dispatch({
        type: 'model/updateState',
        payload: {
          currentModel: item
        }
      });
      dispatch({
        type: 'app/updateState',
        payload: {
          photos: item.photosId,
        }
      });
      router.push(`/catalog/model/${item.id}`)
    };
    const openModalDelete = (id) => {
      dispatch({
        type: 'model/updateState',
        payload: {
          deleteModal: !deleteModal,
          currentModel: id
        }
      })
    };
    const deleteModel = () => {
      dispatch({
        type: 'model/deleteModel',
        payload: {
          id: currentModel
        }
      })
    };


    return (
      <div>
        <SideBarAdmin/>
        <div className="container">
          <div className="row">
            <div className="col mt-5">
              <h1 className="my-4 text-cente">Add Model</h1>
              <div className="row">
                <div className="col-8">
                  <button className="btn btn-success" onClick={addModel}>Add AddModel</button>
                </div>

                <div className="col-1 text-right mr-5">
                  <select className="px-3 py-2 border-primary rounded" onChange={selectSize}>
                    <option value="2">2</option>
                    <option value="5">5</option>
                    <option value="5">10(max)</option>
                  </select>
                </div>
                <div className="col-1">
                  <button className="btn btn-dark" onClick={(e) => getModels(e, page)}>Filter</button>
                </div>
              </div>

              {models.length === 0 ? <h1>Model mavjud emas</h1> :
                <table className="table text-center mt-2">
                  <thead className="thead-light">
                  <tr>
                    <th>#</th>
                    <th>ModelName</th>
                    <th>Brand name</th>
                    <th>Product name</th>
                    <th>Option</th>
                    <th>Price</th>
                    <th>Action</th>
                  </tr>
                  </thead>
                  <tbody>
                  {models.map((item, i) =>
                    <tr key={item.id}>
                      <td>{i + 1}</td>
                      <td>{item.name}</td>
                      <td>{item.brandName}</td>
                      <td>{item.productNameUz}</td>
                      <td>{item.option}</td>
                      <td>{item.price}</td>
                      <td>
                        <button className="btn btn-primary" onClick={() => getModel(item)}>Edit
                        </button>
                        <button className="btn btn-danger" onClick={() => openModalDelete(item.id)}>Delete</button>
                      </td>
                    </tr>
                  )}
                  </tbody>
                </table>
              }
            </div>
          </div>
          <Row className="mt-5 mb-5">
            <Col md={2} className="offset-5">
              <Pagination aria-label="Page navigation example">
                <PaginationItem disabled={page === 0}>
                  <PaginationLink first onClick={(e) => getModels(e, 0)}/>
                </PaginationItem>
                <PaginationItem disabled={page === 0}>
                  <PaginationLink previous onClick={(e) => getModels(e, page - 1)}/>
                </PaginationItem>
                {paginations.map(i =>
                  <PaginationItem key={i} active={page === (i - 1)}
                                  disabled={page === (i - 1) || i === '...'}>
                    <PaginationLink onClick={(e) => getModels(e, (i - 1))}>
                      {i}
                    </PaginationLink>
                  </PaginationItem>)}
                <PaginationItem disabled={(totalPages - 1) === page}>
                  <PaginationLink next onClick={(e) => getModels(e, page + 1)}/>
                </PaginationItem>
                <PaginationItem disabled={page === (totalPages - 1)}>
                  <PaginationLink last onClick={(e) => getModels(e, (totalPages - 1))}/>
                </PaginationItem>
              </Pagination>
            </Col>
          </Row>
          <div className="row">
            <div className="col">
              <Modal isOpen={deleteModal} toggle={openModalDelete}>
                <ModalHeader>
                  <h2>Delete country?</h2>
                </ModalHeader>
                <button onClick={deleteModel}>Ha</button>
                <button onClick={openModalDelete}>yo'q</button>
              </Modal>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

Model.propTypes = {};

export default Model;
