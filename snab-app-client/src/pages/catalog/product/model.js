import api from 'api'
import {toast} from "react-toastify";
const {getProduct, addProduct, editProduct, deleteProduct} = api;

export default {
  namespace: 'product',
  state: {
    currentProduct: {},
    isShowModal: false,
    deleteModal: false,
    deletedId: '',
    products:[]

  },
  subscriptions: {},
  effects: {
    * addProduct({payload}, {call, put, select}) {
        const res = yield call(addProduct, payload);
        if (res.success) {
          yield put({
            type: 'updateState',
            payload: {
              isShowModal: false
            }
          });
          yield put({
            type: 'app/getProducts'
          });
          toast.success(res.message);
        }else{
          toast.error(res.message);
        }
    },
    * editProduct({payload}, {call, put, select}) {
      const res = yield call(editProduct, payload);
      if (res.success) {
        yield put({
          type: 'updateState',
          payload: {
            currentProduct: {},
            isShowModal: false
          }
        });
        yield put({
          type: 'app/getProducts'
        });
        toast.success(res.message);
      }else{
        toast.error(res.message);
      }
    },
    * deleteProduct({payload}, {call, put, select}) {
      const res = yield call(deleteProduct, payload);
      if (res.success) {
        yield put({
          type: 'updateState',
          payload: {
            deleteModal: false,
            deletedId: ''
          }
        });
        yield put({
          type:'app/getProducts'
        });
        toast.success(res.message);
      }else{
        toast.error(res.message);
      }
    },
  },
  reducers: {
    updateState(state, {payload}) {
      return {
        ...state,
        ...payload
      }
    }
  }
}
