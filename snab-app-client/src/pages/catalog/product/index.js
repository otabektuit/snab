import React, {Component} from 'react';
import {connect} from "react-redux";
import {AvField, AvForm} from "availity-reactstrap-validation";
import {Modal, ModalBody, ModalHeader} from "reactstrap";
import SideBarAdmin from "../../../component/SideBarAdmin";

@connect(({product, app}) => ({product, app}))
class Product extends Component {
  componentDidMount() {
    const {dispatch} = this.props;
    dispatch({
      type: 'app/getProducts'
    });
    dispatch({
      type: 'app/getCategories'
    })
  }

  render() {

    const {dispatch, product, app} = this.props;
    const {categories, products} = app;
    const {isShowModal, deleteModal, deletedId, currentProduct} = product;


    const openModal = (item) => {
      dispatch({
        type: 'product/updateState',
        payload: {
          isShowModal: !isShowModal,
          currentProduct: item
        }
      })
    };

    const save = (e, v) => {
        if (currentProduct.id) {
          dispatch({
            type: 'product/editProduct',
            payload: {
              path: currentProduct.id,
              ...v
            }
          })
        }
        else {
          dispatch({
            type: 'product/addProduct',
            payload: {
              ...v
            }
          })
        }

    };

    const filterByCategory = (e) => {
      if (e.target.value !== 'Tanlang') {
        dispatch({
          type: 'app/filterProductByCategory',
          payload: {
            id: e.target.value
          }
        })
      }
      dispatch({
        type: 'app/getProducts'
      })
    };

    const openModalDelete = (id) => {
      dispatch({
        type: 'product/updateState',
        payload: {
          deletedId: id,
          deleteModal: !deleteModal
        }
      })
    };

    const deleteRegion = () => {
      dispatch({
        type: 'product/deleteProduct',
        payload: {
          id: deletedId,
          deleteModal: false
        }
      })
    };


    return (
      <div>
        <SideBarAdmin/>
        <div className="container d-inline-block">
          <div className="row justify-content-center">
            <div className="col-md-8 offset-md-2">
              <h1>Products</h1>
              <AvForm className='row'>
                <AvField type="select" placeholder="nameEn ni kiriting" className='col-md-8 offset-md-1'
                         onChange={filterByCategory} name="category">
                  <option value="Tanlang">Categoryni tanlang</option>
                  {categories.map(item =>
                    <option value={item.id}>{item.nameUz}</option>
                  )}
                </AvField>
              </AvForm>

              <button onClick={openModal} className='btn-primary btn mb-3'>Add Product</button>

              <br/>
              {products.length > 0 ?
                <table border="1" className='table table-bordered table-striped'>
                  <thead>
                  <tr>
                    <th>Tr</th>
                    <th>NomiUz</th>
                    <th>NomiRu</th>
                    <th>NomiEn</th>
                    <th>Category</th>
                    <th>Action</th>
                  </tr>
                  </thead>
                  <tbody>
                  {products.map((item, i) =>
                    <tr>
                      <td>{i + 1}</td>
                      <td>{item.nameUz}</td>
                      <td>{item.nameRu}</td>
                      <td>{item.nameEn}</td>
                      <td>{item.categoryNameUz}</td>
                      <td>
                        <button onClick={() => openModal(item)} className='btn btn-success'>edit</button>
                        <button onClick={() => openModalDelete(item.id)} className='btn btn-danger'>delete</button>
                      </td>
                    </tr>
                  )}
                  </tbody>
                </table> :
                <h1>Product mavjud emas</h1>
              }


              <Modal isOpen={isShowModal} toggle={openModal}>
                <ModalHeader>
                  <h2>Add Product</h2>
                </ModalHeader>
                <ModalBody>
                  <AvForm onValidSubmit={save}>
                    <AvField defaultValue={currentProduct.nameUz} placeholder="nameUz ni kiriting" name="nameUz"/>
                    <AvField defaultValue={currentProduct.nameRu} placeholder="nameRu ni kiriting" name="nameRu"/>
                    <AvField defaultValue={currentProduct.nameEn} placeholder="nameEn ni kiriting" name="nameEn"/>
                    <AvField type="select" placeholder="nameEn ni kiriting" name="category">
                      <option value="tanlang">Categoryni tanlang</option>
                      {categories.map(item =>
                        <option value={`/api/category/${item.id}`}>{item.nameUz}</option>
                      )}
                    </AvField>
                    <button type="submit">Saqlash</button>
                  </AvForm>
                </ModalBody>
              </Modal>

              <Modal isOpen={deleteModal} toggle={openModalDelete}>
                <ModalHeader>
                  <h2>Delete Product?</h2>
                </ModalHeader>
                <button onClick={deleteRegion} className='btn btn-danger'>Delete</button>
                <button onClick={openModalDelete} className='btn btn-secondary'>yo'q</button>
              </Modal>

            </div>
          </div>
        </div>
      </div>
    );
  }
}

Product.propTypes = {};

export default Product;
