import React, {Component} from 'react';
import {AvField, AvForm} from "availity-reactstrap-validation";
import {Modal, ModalBody, ModalHeader} from "reactstrap";
import {connect} from "react-redux";
import SideBarAdmin from "../../../component/SideBarAdmin";

@connect(({dashboard, app}) => ({dashboard, app}))

class Dashboard extends Component {
  componentDidMount() {
    const {dispatch}=this.props;
    dispatch({
      type:'app/getDashboards'
    })
  }

  render() {
    const {dispatch,app,dashboard}=this.props;
    const {dashboards,photoId}=app;
    const {isShowModal, deleteModal, deletedId, currentDashboard} = dashboard;
    const openModal = (item) => {
      dispatch({
        type: 'dashboard/updateState',
        payload: {
          isShowModal: !isShowModal,
          currentDashboard: item
        }
      })
    };

    const save = (e, v) => {
      if (currentDashboard.id) {
        dispatch({
          type: 'dashboard/edit',
          payload: {
            path: currentDashboard.id,
            ...v,
            attachmentId:photoId
          }
        })
      } else {
        dispatch({
          type: 'dashboard/save',
          payload: {
            ...v,
            attachmentId:photoId
          }
        })
      }
    };

    const uploadPhotos = (e) => {
      if (e.target.files[0]) {
        dispatch({
          type: 'app/uploadPhoto',
          payload: {
            fileUpload: true,
            file: e.target.files
          }
        }).then(res => {
            if (res.success) {
              dispatch({
                type: 'app/updateState',
                payload: {
                  photoId: res.object
                }
              })
            }
          }
        )
      }
    };

    const openModalDelete = (id) => {
      dispatch({
        type: 'dashboard/updateState',
        payload: {
          deletedId: id,
          deleteModal: !deleteModal
        }
      })
    };

    const deleteDashboard = () => {
      dispatch({
        type: 'dashboard/delete',
        payload: {
          id: deletedId,
          deleteModal: false
        }
      })
    };

    return (
      <div>
        <SideBarAdmin/>
        <div className="container">
          <div className="row">
            <div className="col mt-5">
              <h1>Dashboard</h1>
              <button onClick={openModal} className='btn-primary btn mb-3'>Add Dashboard</button>

              <br/>
              {dashboards.length > 0 ?
                <table border="1" className='table table-bordered table-striped'>
                  <thead>
                  <tr>
                    <th>Tr</th>
                    <th>NameUz</th>
                    <th>DescriptionUz</th>
                    <th>AboutUz</th>
                    <th>Image</th>
                    <th>Action</th>
                  </tr>
                  </thead>
                  <tbody>
                  {dashboards.map((item, i) =>
                    <tr>
                      <td>{i + 1}</td>
                      <td>{item.nameUz}</td>
                      <td>{item.descrpUz}</td>
                      <td>{item.aboutUz}</td>
                      <td><img src={`/api/attach/${item.attachmentId}`} className="img-fluid"   width="50" height="50" alt="Bu yerda rasm"/></td>
                      <td>
                        <button onClick={() => openModal(item)} className='btn btn-success'>edit</button>
                        <button onClick={() => openModalDelete(item.id)} className='btn btn-danger'>delete</button>
                      </td>
                    </tr>
                  )}
                  </tbody>
                </table> :
                <h1>Dashboard mavjud emas</h1>
              }


              <Modal isOpen={isShowModal} toggle={openModal}>
                <ModalHeader>
                  <h2>Add Dashboard</h2>
                </ModalHeader>
                <ModalBody>
                  <AvForm onValidSubmit={save}>
                    <AvField defaultValue={currentDashboard.nameUz} placeholder="name UZ ni kiriting" name="nameUz"/>
                    <AvField defaultValue={currentDashboard.nameRu} placeholder="name RU ni kiriting" name="nameRu"/>
                    <AvField defaultValue={currentDashboard.nameEn} placeholder="name EN ni kiriting" name="nameEn"/>
                    <AvField defaultValue={currentDashboard.descrpUz} placeholder="description UZ ni kiriting" name="descrpUz"/>
                    <AvField defaultValue={currentDashboard.descrpRu} placeholder="description RU ni kiriting" name="descrpRu"/>
                    <AvField defaultValue={currentDashboard.descrpEn} placeholder="description EN ni kiriting" name="descrpEn"/>
                    <AvField defaultValue={currentDashboard.aboutUz} placeholder="about UZ ni kiriting" name="aboutUz"/>
                    <AvField defaultValue={currentDashboard.aboutRu} placeholder="about RU ni kiriting" name="aboutRu"/>
                    <AvField defaultValue={currentDashboard.aboutEn} placeholder="about EN ni kiriting" name="aboutEn"/>
                    <div className="custom-file mb-3">
                      <AvField name="photoId" className="custom-file-input" onChange={uploadPhotos} type="file"/>
                      <label className="custom-file-label" htmlFor="customFile">Choose file</label>
                    </div>
                    <button type="submit" className="btn btn-success">Saqlash</button>
                  </AvForm>
                </ModalBody>
              </Modal>
              <Modal isOpen={deleteModal} toggle={openModalDelete}>
                <ModalHeader>
                  <h2>Delete Dashboard?</h2>
                </ModalHeader>
                <button onClick={deleteDashboard} className='btn btn-danger'>Delete</button>
                <button onClick={openModalDelete} className='btn btn-secondary'>Back</button>
              </Modal>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

Dashboard.propTypes = {};

export default Dashboard;
