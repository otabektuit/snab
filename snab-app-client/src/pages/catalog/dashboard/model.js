import api from 'api'
import {toast} from "react-toastify";

const {getDashboard, addDashboard, editDashboard, deleteDashboard} = api;

export default {
  namespace: 'dashboard',
  state: {
    currentDashboard:{},
    isShowModal: false,
    deleteModal: false,
    deletedId: '',
  },
  subscriptions: {},
  effects: {
    * save({payload}, {call, put, select}) {
      const res = yield call(addDashboard,payload);
      if (res.success) {
        yield put({
          type: 'updateState',
          payload: {
            isShowModal: false
          }
        });
        yield put({
          type: 'app/getDashboards'
        });
        toast.success(res.message);
      }else{
        toast.error(res.message);
      }
    },
    * edit({payload}, {call, put, select}) {
      const res = yield call(editDashboard, payload);
      if (res.success) {
        yield put({
          type: 'updateState',
          payload: {
            isShowModal: false
          }
        });
        yield put({
          type: 'app/getDashboards'
        });
        toast.success(res.message);
      }else {
        toast.error(res.message);
      }
    },
    * delete({payload}, {call, put, select}) {
      const res = yield call(deleteDashboard,payload);
      if(res.success){
        yield put({
          type:'updateState',
          payload:{
            deleteModal: false,
            deletedId: ''
          }
        });
        yield put({
          type:'app/getDashboards'
        });
        toast.success(res.message);
      }
      else {
        toast.error(res.message);
      }
    },
  },
  reducers: {
    updateState(state, {payload}) {
      return {
        ...state,
        ...payload
      }
    }
  }
}
