import api from 'api'
import {toast} from "react-toastify";

const {getBrand, addBrand, editBrand, deleteBrand} = api;

export default {
  namespace: 'brand',
  state: {
    currentBrand: {},
    isShowModal: false,
    deleteModal: false,
    deletedId: '',

  },
  subscriptions: {},
  effects: {
    * addBrand({payload}, {call, put, select}) {
      const res = yield call(addBrand,payload);
      if (res.success) {
        yield put({
          type: 'updateState',
          payload: {
            isShowModal: false
          }
        });
        yield put({
          type: 'app/getBrands'
        });
        toast.success(res.message);
      }else{
        toast.error(res.message);
      }
    },
    * editBrand({payload}, {call, put, select}) {
      const res = yield call(editBrand, payload);
      if (res.success) {
        yield put({
          type: 'updateState',
          payload: {
            isShowModal: false
          }
        });
        yield put({
          type: 'app/getBrands'
        });
        toast.success(res.message);
      }else {
        toast.error(res.message);
      }
    },
    * deleteBrand({payload}, {call, put, select}) {
      const res = yield call(deleteBrand,payload);
      if(res.success){
        yield put({
          type:'updateState',
          payload:{
            deleteModal: false,
            deletedId: ''
          }
        });
        yield put({
          type:'app/getBrands'
        });
        toast.success(res.message);
      }
      else {
        toast.error(res.message);
      }
    },
  },
  reducers: {
    updateState(state, {payload}) {
      return {
        ...state,
        ...payload
      }
    }
  }
}
