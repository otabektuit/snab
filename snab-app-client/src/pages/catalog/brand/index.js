import React, {Component} from 'react';
import {connect} from "react-redux";
import {AvField, AvForm} from "availity-reactstrap-validation";
import {Modal, ModalBody, ModalHeader} from "reactstrap";
import SideBarAdmin from "../../../component/SideBarAdmin";
import * as router from "umi/router";

@connect(({brand, app}) => ({brand, app}))
class Brand extends Component {
  componentDidMount() {
    const {dispatch} = this.props;
    dispatch({
      type: 'app/getBrands'
    });
    dispatch({
      type: 'app/getProducts'
    })
  }

  render() {

    const {dispatch, brand, app} = this.props;
    const {brands, products,currentUser} = app;
    const {isShowModal, deleteModal, deletedId, currentBrand} = brand;

    const openModal = (item) => {
      dispatch({
        type: 'brand/updateState',
        payload: {
          isShowModal: !isShowModal,
          currentBrand: item
        }
      })
    };

    const save = (e, v) => {
      if (currentBrand.id) {
        dispatch({
          type: 'brand/editBrand',
          payload: {
            path: currentBrand.id,
            ...v
          }
        })
      } else {
        dispatch({
          type: 'brand/addBrand',
          payload: {
            ...v
          }
        })
      }
    };

    const filterByProduct = (e) => {
      if (e.target.value === 'Tanlang') {
        dispatch({
          type: 'app/getBrands'
        })
      } else {
        dispatch({
          type: 'app/filterBrandByProduct',
          payload: {
            id: e.target.value
          }
        })
      }
    };

    const openModalDelete = (id) => {
      dispatch({
        type: 'brand/updateState',
        payload: {
          deletedId: id,
          deleteModal: !deleteModal
        }
      })
    };

    const deleteBrand = () => {
      dispatch({
        type: 'brand/deleteBrand',
        payload: {
          id: deletedId,
          deleteModal: false
        }
      })
    };





    return (<div>
      <SideBarAdmin/>
      <div className="container d-inline-block">
        <div className="row justify-content-center">
          <div className="col-md-8 offset-md-2">
            <h1>Brands</h1>
            <AvForm className='row'>
              <AvField type="select" placeholder="name ni kiriting" className='col-md-8 offset-md-1'
                       onChange={filterByProduct} name="category">
                <option value="Tanlang">Productni tanlang</option>
                {products.map(item =>
                  <option value={item.id}>{item.nameUz}</option>
                )}
              </AvField>
            </AvForm>

            <button onClick={openModal} className='btn-primary btn mb-3'>Add Brand</button>

            <br/>
            {brands.length > 0 ?
              <table border="1" className='table table-bordered table-striped'>
                <thead>
                <tr>
                  <th>Tr</th>
                  <th>Nomi</th>
                  <th>Category</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
                {brands.map((item, i) =>
                  <tr>
                    <td>{i + 1}</td>
                    <td>{item.name}</td>
                    <td>{item.productNameUz}</td>
                    <td>
                      <button onClick={() => openModal(item)} className='btn btn-success'>edit</button>
                      <button onClick={() => openModalDelete(item.id)} className='btn btn-danger'>delete</button>
                    </td>
                  </tr>
                )}
                </tbody>
              </table> :
              <h1>Brand mavjud emas</h1>
            }


            <Modal isOpen={isShowModal} toggle={openModal}>
              <ModalHeader>
                <h2>Add Brand</h2>
              </ModalHeader>
              <ModalBody>
                <AvForm onValidSubmit={save}>
                  <AvField defaultValue={currentBrand.nameUz} placeholder="name ni kiriting" name="name"/>
                  <AvField type="select" placeholder="nameEn ni kiriting" name="product">
                    <option value="tanlang">Productni tanlang</option>
                    {products.map(item =>
                      <option value={`/api/product/${item.id}`}>{item.nameUz}</option>
                    )}
                  </AvField>
                  <button type="submit">Saqlash</button>
                </AvForm>
              </ModalBody>
            </Modal>

            <Modal isOpen={deleteModal} toggle={openModalDelete}>
              <ModalHeader>
                <h2>Delete Brand?</h2>
              </ModalHeader>
              <button onClick={deleteBrand} className='btn btn-danger'>Delete</button>
              <button onClick={openModalDelete} className='btn btn-secondary'>Back</button>
            </Modal>

          </div>
        </div>
      </div>
    </div>

    );
  }
}

Brand.propTypes = {};

export default Brand;
