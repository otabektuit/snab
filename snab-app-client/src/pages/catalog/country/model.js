import api from 'api'
import {toast} from "react-toastify";

const {getCountries, getCountry, addCountry, editCountry, deleteCountry} = api;

export default ({
  namespace: 'country',
  state: {
    countries: [],
    modalniOchish: false,
    currentCountry: {},
    deleteModal: false,
    deletedId: ''
  },
  subscriptions: {},
  effects: {
    * saveCountry({payload}, {call, put, select}) {
      const res = yield call(addCountry, payload);
      if (res.success) {
        yield put({
          type: 'updateState',
          payload: {
            modalniOchish: false
          }
        });
        yield put({
          type: 'app/getCountries'
        });
        toast.success(res.message);
      }else{
        toast.error(res.message);
      }
    },
    * editCountry({payload}, {call, put, select}) {
      const res = yield call(editCountry, payload);
      if (res.success) {
        yield put({
          type: 'updateState',
          payload: {
            modalniOchish: false,
            currentCountry: {}
          }
        });
        yield put({
          type: 'app/getCountries'
        });
        toast.success(res.message);
      }else{
        toast.error(res.message);
      }

    },
    * deleteCountry({payload}, {call, put, select}) {
      const res = yield call(deleteCountry, payload);
      if (res.success) {
        yield put({
          type: 'updateState',
          payload: {
            deleteModal: false,
            deletedId: ''
          }
        });
        yield put({
          type: 'app/getCountries'
        });
        toast.success(res.message);
      }
      else{
        toast.error(res.message);
      }
    },
  },
  reducers: {
    updateState(state, {payload}) {
      return {
        ...state,
        ...payload
      }
    }
  }
})
