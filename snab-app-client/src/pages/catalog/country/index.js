import React, {Component} from 'react';
import {connect} from "react-redux";
import {Modal, ModalBody, ModalHeader} from "reactstrap";
import {AvField, AvForm} from 'availity-reactstrap-validation'
import SideBarAdmin from "../../../component/SideBarAdmin";

@connect(({country, app}) => ({country, app}))
class Country extends Component {
  componentDidMount() {
    const {dispatch} = this.props;
    dispatch({
      type: 'app/getCountries'
    })
  }

  render() {
    const {dispatch, country, app} = this.props;
    const {modalniOchish, currentCountry, deleteModal, deletedId} = country;
    const {countries} = app;

    const save = (e, v) => {
      if (currentCountry.id) {
        dispatch({
          type: 'country/editCountry',
          payload: {
            path: currentCountry.id,
            ...v
          }
        })
      } else {
        dispatch({
          type: 'country/saveCountry',
          payload: {
            ...v
          }
        })
      }

    };

    const openModal = (item) => {
      dispatch({
        type: 'country/updateState',
        payload: {
          modalniOchish: !modalniOchish,
          currentCountry: item
        }
      })
    };

    const openModalDelete = (id) => {
      dispatch({
        type: 'country/updateState',
        payload: {
          deletedId: id,
          deleteModal: !deleteModal
        }
      })
    };

    const deleteCountry = () => {
      dispatch({
        type: 'country/deleteCountry',
        payload: {
          id: deletedId
        }
      })
    };

    return (
      <div>
        <SideBarAdmin/>
        <div className="d-inline-block container">
          <div className="row justify-content-center">
            <div className="col-md-8 offset-md-2">
              <h1 className="text">Country</h1>
              <button className="btn btn-primary mb-3" onClick={openModal}>Add Country</button>
              <table border="1" className='table table-bordered table-striped'>
                <thead>
                <tr>
                  <th>Tr</th>
                  <th>NomiUz</th>
                  <th>NomiRu</th>
                  <th>NomiEn</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
                {countries.map((item, i) =>
                  <tr>
                    <td>{i + 1}</td>
                    <td>{item.nameUz}</td>
                    <td>{item.nameRu}</td>
                    <td>{item.nameEn}</td>
                    <td>
                      <button onClick={() => openModal(item)} className='btn btn-success'>edit</button>
                      <button onClick={() => openModalDelete(item.id)} className='btn btn-danger'>delete</button>
                    </td>
                  </tr>
                )}
                </tbody>
              </table>


              <Modal isOpen={modalniOchish} toggle={openModal}>
                <ModalHeader>
                  <h2>Add country</h2>
                </ModalHeader>
                <ModalBody>
                  <AvForm onValidSubmit={save}>
                    <AvField defaultValue={currentCountry.nameUz} placeholder="nameUz ni kiriting" name="nameUz"/>
                    <AvField defaultValue={currentCountry.nameRu} placeholder="nameRu ni kiriting" name="nameRu"/>
                    <AvField defaultValue={currentCountry.nameEn} placeholder="nameEn ni kiriting" name="nameEn"/>
                    <button type="submit">Saqlash</button>
                  </AvForm>
                </ModalBody>
              </Modal>

              <Modal isOpen={deleteModal} toggle={openModalDelete}>
                <ModalHeader>
                  <h2>Delete country?</h2>
                </ModalHeader>
                <button onClick={deleteCountry} className='btn btn-danger'>Delete</button>
                <button onClick={openModalDelete} className='btn btn-secondary'>Back</button>
              </Modal>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

Country.propTypes = {};

export default Country;
