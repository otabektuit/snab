import React, {Component} from 'react';
import {Link} from "react-router-dom";
import * as router from "umi/router";
import {connect} from "react-redux";
import SideBarAdmin from "../../component/SideBarAdmin";

@connect(({app}) => ({app}))
class Catalog extends Component {


  render() {
    const {app} = this.props;
    const {currentUser} = app;
    return (
      currentUser.authorities ?
        (currentUser.authorities.filter(i => i.roleName === 'ROLE_ADMIN' || i.roleName === 'ROLE_USER').length > 1 ?
            <div>
              <SideBarAdmin/>
              <div className="container">
                <div className="row justify-content-center">
                  <div className="col-md-8 offset-md-2">
                    <h1 className="text-secondary">Catalog</h1>
                  </div>
                </div>
              </div>
            </div>
            :
            currentUser.authorities.filter(i => i.roleName === 'ROLE_USER').length === 1 ?
              router.push('/')
              :
              router.push('/')
        )
        :
        router.push('/')
    );
  }
}

Catalog.propTypes = {};

export default Catalog;
