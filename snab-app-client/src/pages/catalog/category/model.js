import api from 'api'
import {toast} from "react-toastify";
const {getCategory, addCategory, editCategory, deleteCategory} = api;

export default ({
  namespace: 'category',
  state: {
    currentCategory: {},
    deletedId: '',
    deleteModal: false,
    isModal: false,
  },
  subscriptions: {},
  effects: {
    * saveCategory({payload}, {call, put, select}) {
      const res = yield call(addCategory, payload);
      if (res.success) {
        yield put({
          type: 'updateState',
          payload: {
            isModal: false
          }
        });
        yield put({
          type: 'app/getCategories'
        });
        toast.success(res.message);
      }else{
        toast.error(res.meessage);
      }
    },
    * editCategory({payload}, {call, put, select}) {
      const res = yield call(editCategory, payload);
      if (res.success) {
        yield put({
          type: 'updateState',
          payload: {
            isModal: false,
            currentCategory: {}
          }
        });
        yield put({
          type: 'app/getCategories'
        });
        toast.success(res.message);
      }else{
        toast.error(res.meessage);
      }
    },
    * deleteCategory({payload}, {call, put, select}) {
      const res=yield call(deleteCategory,payload);
      if (res.success){
        yield put({
          type:'updateState',
          payload:{
            deleteModal: false,
            deletedId: ''
          }
        });
        yield put({
          type:'app/getCategories'
        });
        toast.success(res.message);
      }else{
        toast.error(res.meessage);
      }
    }

  },
  reducers: {
    updateState(state, {payload}) {
      return {
        ...state,
        ...payload
      }
    }
  }
})
