import React, {Component} from 'react';
import {connect} from "react-redux";
import {Modal, ModalBody, ModalHeader} from "reactstrap";
import {AvField, AvForm} from "availity-reactstrap-validation";
import SideBarAdmin from "../../../component/SideBarAdmin";

@connect(({category, app}) => ({category, app}))
class Category extends Component {
  componentDidMount() {
    const {dispatch} = this.props;
    dispatch({
      type: 'app/getCategories'
    })
  }

  render() {

    const {dispatch, app, category} = this.props;
    const {isModal, currentCategory, deletedId, deleteModal} = category;
    const {categories} = app;
    const save = (e, v) => {
      if (currentCategory.id) {
        dispatch({
          type: 'category/editCategory',
          payload: {
            path: currentCategory.id,
            ...v
          }
        })
      }
      else {
        dispatch({
          type: 'category/saveCategory',
          payload: {
            ...v
          }
        })
      }

    };
    const openModal = (item) => {
      dispatch({
        type: 'category/updateState',
        payload: {
          isModal: !isModal,
          currentCategory: item
        }
      })
    };
    const openModalDelete = (id) => {
      dispatch({
        type: 'category/updateState',
        payload: {
          deletedId: id,
          deleteModal: !deleteModal
        }
      })
    };
    const deleteCategory = () => {
      dispatch({
        type: 'category/deleteCategory',
        payload: {
          id:deletedId
        }
      })
    };


    return (
      <div>

        <SideBarAdmin/>
        <div className="d-inline-block container">
          <div className="row justify-content-center">
            <div className="col-md-8 offset-md-2">
              <h1 className="text-secondary">Category</h1>
              <button className="btn btn-primary mb-3" onClick={openModal}>Add Country</button>
              {
                categories.length>0?
                  <table border="1" className='table table-bordered table-striped'>
                    <thead>
                    <tr>
                      <th>Tr</th>
                      <th>NomiUz</th>
                      <th>NomiRu</th>
                      <th>NomiEn</th>
                      <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    {categories.map((item, i) =>
                      <tr>
                        <td>{i + 1}</td>
                        <td>{item.nameUz}</td>
                        <td>{item.nameRu}</td>
                        <td>{item.nameEn}</td>
                        <td>
                          <button onClick={() => openModal(item)} className='btn btn-success'>edit</button>
                          <button onClick={() => openModalDelete(item.id)} className='btn btn-danger'>delete</button>
                        </td>
                      </tr>
                    )}</tbody>
                  </table>
                  :
                  <h4>
                    Category doesn't exist
                  </h4>
              }

              <Modal isOpen={isModal} toggle={openModal}>
                <ModalHeader>
                  <h2>Add country</h2>
                </ModalHeader>
                <ModalBody>
                  <AvForm onValidSubmit={save}>
                    <AvField defaultValue={currentCategory.nameUz} placeholder="nameUz ni kiriting" name="nameUz"/>
                    <AvField defaultValue={currentCategory.nameRu} placeholder="nameRu ni kiriting" name="nameRu"/>
                    <AvField defaultValue={currentCategory.nameEn} placeholder="nameEn ni kiriting" name="nameEn"/>
                    <button type="submit">Saqlash</button>
                  </AvForm>
                </ModalBody>
              </Modal>

              <Modal isOpen={deleteModal} toggle={openModalDelete}>
                <ModalHeader>
                  <h2>Delete country?</h2>
                </ModalHeader>
                <button onClick={deleteCategory} className='btn btn-danger'>Delete</button>
                <button onClick={openModalDelete} className='btn btn-secondary'>Back</button>
              </Modal>
            </div>
          </div>
        </div>
      </div>

    );
  }
}

Category.propTypes = {};

export default Category;
