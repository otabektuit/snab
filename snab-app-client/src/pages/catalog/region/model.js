import api from 'api'
import {toast} from "react-toastify";

const {getRegions, getRegion, addRegion, editRegion, deleteRegion, filterCountryByRegion} = api;

export default {
  namespace: 'region',
  state: {
    regions: [],
    currentRegion:{},
    isShowModal: false,
    deleteModal: false,
    deletedId: ''

  },
  subscriptions: {},
  effects: {
    * addRegion({payload}, {call, put, select}) {
      const res = yield call(addRegion, payload);
        if (res.success) {
          yield put({
            type: 'updateState',
            payload: {
              isShowModal: false
            }
          });
          yield put({
            type: 'app/getRegions'
          });
          toast.info(res.message);
        }else{
          toast.error(res.message);
        }
    },
    * editRegion({payload},{call,put,select}){
      const res = yield call(editRegion,payload);
      if(res.success){
        yield put({
          type:'updateState',
          payload:{
            modalniOchish:false,
            currentRegion:{}
          }
        });
        yield put({
          type:'app/getRegions'
        });
        toast.success(res.message);
      }else{
        toast.error(res.message);
      }
    },
    * deleteRegion({payload},{call,put,select}){
      const res = yield call(deleteRegion,payload);
      if(res.success){
        yield put({
          type:'updateState',
          payload:{
            deleteModal: false,
            deletedId: ''
          }
        });
        yield put({
          type: 'app/getRegions'
        });
        toast.success(res.message);
      }else{
        toast.error(res.message);
      }
    }

  },
  reducers: {
    updateState(state, {payload}) {
      return {
        ...state,
        ...payload
      }
    }
  }
}
