import React, {Component} from 'react';
import {connect} from "react-redux";
import {Modal, ModalBody, ModalHeader} from "reactstrap";
import {AvField, AvForm} from "availity-reactstrap-validation";
import SideBarAdmin from "../../../component/SideBarAdmin";

@connect(({region, app}) => ({region, app}))
class Region extends Component {
  componentDidMount() {
    const {dispatch} = this.props;
    dispatch({
      type: 'app/getRegions'
    });
    dispatch({
      type: 'app/getCountries'
    })
  }

  render() {
    const {region, app, dispatch} = this.props;
    const {isShowModal,deleteModal,deletedId,currentRegion} = region;
    const {countries,regions} = app;

    const openModal = () => {
      dispatch({
        type: 'region/updateState',
        payload: {
          isShowModal: !isShowModal
        }
      })
    };
    const save = (e, v) => {
      if (v) {
        if (currentRegion.id) {
          dispatch({
            type: 'region/editRegion',
            payload: {
              path: currentRegion.id,
              ...v
            }
          })
        } else {
          dispatch({
            type: 'region/addRegion',
            payload: {
              ...v
            }
          })
        }
      }else {
        alert('adasad')
      }
    };
    const filterByCountry = (e) => {
        if (e.target.value === 'Tanlang') {
          dispatch({
            type: 'app/getRegions'
          })
        } else {
          dispatch({
            type: 'app/filterCountryByRegion',
            payload: {
              id: e.target.value
            }
          })
        }
      };

    const openModalDelete = (id) => {
      dispatch({
        type: 'region/updateState',
        payload: {
          deletedId: id,
          deleteModal: !deleteModal
        }
      })
    };

    const deleteRegion = () => {
      dispatch({
        type: 'region/deleteRegion',
        payload: {
          id: deletedId,
          deleteModal:false
        }
      })
    };


    return (
      <div>

        <SideBarAdmin/>

        <div className="container d-inline-block">
          <div className="row justify-content-center">
            <div className="col-md-8 offset-md-2">
              <h1 className='text-secondary'>Regions</h1>
            <AvForm className='row'>
              <AvField type="select" className='col-md-8 offset-md-1' placeholder="nameEn ni kiriting" onChange={filterByCountry} name="country">
                <option value="Tanlang">Davlatni tanlang</option>
                {countries.map(item =>
                  <option value={item.id}>{item.nameUz}</option>
                )}
              </AvField></AvForm>

            <button onClick={openModal} className='btn btn-primary mb-3'>Add Region</button>

            <br/>
            {regions.length > 0 ?
              <table border="1" className='table-bordered table table-striped'>
                <thead>
                <tr>
                  <th>Tr</th>
                  <th>NomiUz</th>
                  <th>NomiRu</th>
                  <th>NomiEn</th>
                  <th>Davlati</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
                {regions.map((item, i) =>
                  <tr>
                    <td>{i + 1}</td>
                    <td>{item.nameUz}</td>
                    <td>{item.nameRu}</td>
                    <td>{item.nameEn}</td>
                    <td>{item.countryNameUz}</td>
                    <td>
                      <button onClick={()=>openModal(item)} className='btn btn-success'>edit</button>
                      <button onClick={()=>openModalDelete(item.id)} className='btn btn-danger'>delete</button>
                    </td>
                  </tr>
                )}
                </tbody>
              </table> :
              <h1>Region mavjud emas</h1>
            }


            <Modal isOpen={isShowModal} toggle={openModal}>
              <ModalHeader>
                <h2>Add Region</h2>
              </ModalHeader>
              <ModalBody>
                <AvForm onValidSubmit={save}>
                  <AvField placeholder="nameUz ni kiriting" name="nameUz"/>
                  <AvField placeholder="nameRu ni kiriting" name="nameRu"/>
                  <AvField placeholder="nameEn ni kiriting" name="nameEn"/>
                  <AvField type="select" placeholder="nameEn ni kiriting" name="country">
                  <option value="tanlang">Davlatni tanlang</option>
                  {countries.map(item =>
                    <option value={`/api/country/${item.id}`}>{item.nameUz}</option>
                  )}
                </AvField>
                  <button type="submit">Saqlash</button>
                </AvForm>
              </ModalBody>
            </Modal>

            <Modal isOpen={deleteModal} toggle={openModalDelete}>
              <ModalHeader>
                <h2>Delete Region?</h2>
              </ModalHeader>
              <button onClick={deleteRegion} className='btn btn-danger'>Delete</button>
              <button onClick={openModalDelete} className='btn btn-secondary'>Back</button>
            </Modal>

            </div>
          </div>
        </div>

      </div>
    );
  }
}

Region
  .propTypes = {};

export default Region;
