import {STORAGE_NAME} from 'utils/constant';
import React, {Component} from 'react';
import {AvField, AvForm} from 'availity-reactstrap-validation'
import {connect} from "react-redux";
import router from "umi/router";
import {Card, Col, Container, Row} from "reactstrap";
import {firebaseAuth} from 'utils/firebase';
import firebase from 'firebase/app';
import {formatMessage, FormattedMessage} from "umi-plugin-locale";

@connect(({auth}) => ({auth}))
class Index extends Component {
  componentDidMount() {
    const {dispatch} = this.props;
    if (localStorage.getItem(STORAGE_NAME)) {
      dispatch({
        type: 'auth/userMe'
      }).then(res => {
        if (res.success) {
          router.push('/cabinet');
        }
      })
    }

    let cont = document.getElementById('reCaptcha');

    this.recaptchaVerifier = new firebase.auth.RecaptchaVerifier(cont, {
      'size': 'invisible',
      'callback': (res) => {
      },
      'expired-callback': () => {
      },
      'error-callback': () => {
        dispatch(
          {
            type: 'app/updateState',
            payload: {
              alert: {
                message: 'Ошибка! Попытайтесь заново'
              }
            },
          }
        )
      }
    });

    this.recaptchaVerifier.render();

    dispatch({
      type: 'auth/updateState',
      payload: {
        reCaptcha: this.recaptchaVerifier
      }
    });
  }


  render() {

    const {dispatch, auth} = this.props;
    const {
      reCaptcha, phoneNumber, firstName, lastName, password,
      email, isShowInputFirebase, confirmationResult, verificationCode
    } = auth;


    const signUp = (e, v) => {
      confirmationResult.confirm(verificationCode)
        .then(
          dispatch({
            type: 'auth/signUp',
            payload: {
              phoneNumber, firstName, lastName, password, email
            }
          })
        )
    };


    const changeState = (e) => {

      dispatch({
        type: 'auth/updateState',
        payload: {
          [e.target.name]: e.target.value
        }
      })
    };

    const sendToFireBase = () => {
      firebaseAuth.signInWithPhoneNumber(phoneNumber, reCaptcha)
        .then((res) =>
          dispatch({
            type: 'auth/updateState',
            payload: {
              isShowInputFirebase: true,
              confirmationResult: res
            }
          })
        )
    };

    return (
      <div className="register-page mt-4">
        <Container>
          <Row>
            <Col md={6} className="offset-md-3">
              <Card className="border-0" >
                {!isShowInputFirebase ?
                  <Row>
                    <Col md={10} className="offset-md-1 card-body text-center bg-light jumbotron shadow" style={{borderRadius:'20px'}}>
                      <h1 className="text-uppercase"><FormattedMessage id="SIGNUP"/></h1>
                      <AvForm className="register">
                        <AvField onChange={changeState} name="firstName" placeholder={formatMessage({id:'USERNAME'})} required/>
                        <AvField onChange={changeState} name="lastName" placeholder={formatMessage({id:'USERSURNAME'})} required/>
                        <AvField onChange={changeState} name="phoneNumber" placeholder={formatMessage({id:'USERPHONENUMBER'})} required/>
                        <AvField onChange={changeState} name="email" placeholder="Email" required/>
                        <AvField onChange={changeState} name="prepassword" type="password" placeholder="*************" required/>
                        <AvField onChange={changeState} name="password" type="password" placeholder="*************" required/>
                        <button className="btn btn-success btn-block mt-2" onClick={sendToFireBase}>
                          <FormattedMessage id="READY"/>
                        </button>
                        <span className="font-weight-semi-bold enter-login2 text-center">
                          <FormattedMessage id="HAVEACCOUNT"/>
                          <a href="/auth/login" className="mt-2">
                            <FormattedMessage id="SIGNIN"/>
                          </a> </span>
                      </AvForm>
                    </Col>
                  </Row>

                  :

                  <Row>
                    <Col md={10} className="offset-md-1 card-body text-center bg-light jumbotron">
                      <AvForm onValidSubmit={signUp}>
                        <AvField onChange={changeState} name="verificationCode" placeholder="_ _ _ _ _ _" style={{lineWidth:"8px",textAlign:'center'}}
                                 required/>
                        <p className="text-secondary text-center">
                          <FormattedMessage id="CODESENT"/>
                        </p>
                        <button type="submit" className="btn btn-outline-info">
                          <FormattedMessage id="SUBMIT"/>
                        </button>
                      </AvForm>
                    </Col>
                  </Row>
                }
                <div id="reCaptcha" render="explicit" style={{display: 'none'}}/>
              </Card>
            </Col>
          </Row>
        </Container>
      </div>
    );
  }
}

Index.propTypes = {};

export default Index;
