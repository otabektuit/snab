import {STORAGE_NAME} from 'utils/constant';
import React, {Component} from 'react';
import {Button, Card, Col, Container, Row} from "reactstrap";
import router from "umi/router";
import {connect} from "dva";
import {AvField, AvForm} from "availity-reactstrap-validation";
import {FormattedMessage} from "umi-plugin-locale";


@connect(({auth}) => ({auth}))
class Index extends Component {
  componentDidMount() {
    const {dispatch} = this.props;
    if (localStorage.getItem(STORAGE_NAME)) {
      dispatch({
        type: 'auth/ketmon'
      }).then(r => {
        if (r.success) {
          router.push('/cabinet');
        }
      })
    }
  }

  render() {
    const {dispatch} = this.props;

    const login = (e, v) => {
      dispatch({
        type: 'auth/signIn',
        payload: {
          ...v
        }
      })
    };

    return (
      <div className="login-page">
        <Container>
          <Row>
            <Col md={6} className="offset-md-3">
              <Card className="border-0">
                <Row>
                  <Col md={10} className="offset-md-1">
                    <div className="shadow" style={{borderRadius:'15px'}}>
                      <h3 className="font-weight-bold enter-login w-100 text-center text-white py-4" style={{borderTopLeftRadius:'15px',borderTopRightRadius:'15px',backgroundColor:'#79de5b'}}>
                        <FormattedMessage id="SIGNIN"/>
                      </h3>
                      <div className="p-3">
                        <AvForm onValidSubmit={login}>
                          <div className="form-group">
                            <AvField type="text" className="mt-2 font-weight-bolder" placeholder="+123456789" name="phoneNumber" style={{borderRadius:'18px'}} required />
                            <AvField type="password" className="font-weight-bolder" placeholder="*********" name="password" style={{borderRadius:'18px'}} required/>
                            <Button type="submit" className="btn btn-block mt-5" style={{borderRadius:'18px',backgroundColor:'#79de5b',borderColor:'transparent'}}>
                              <FormattedMessage id="SIGNIN"/>
                            </Button>
                          </div>
                        </AvForm>
                        <div className="register">
                          <span className="font-weight-semi-bold enter-login2 text-center">
                            <FormattedMessage id="HAVENOTACCOUNT"/>
                          <a href="/auth/register" className="mt-2">
                            <FormattedMessage id="SIGNUP"/>
                          </a>
                          </span>
                        </div>
                      </div>

                    </div>
                  </Col>
                </Row>

              </Card>
            </Col>
          </Row>
        </Container>
      </div>
    );
  }
}

Index.propTypes = {};

export default Index;
