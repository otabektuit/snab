import api from 'api'
import {STORAGE_NAME} from 'utils/constant';
import {TOKEN_NAME} from "../../constants";
import {router} from 'umi';

const {signIn,userMe,signUp} = api;

export default ({
  namespace: 'auth',
  state: {
    currentUser: {},
    phoneNumber: '',
    password: '',
    email: '',
    lastName: '',
    firstName: '',
    isShowInputFirebase: false,
    confirmationResult: null,
    reCaptcha: null,
    verificationCode:''
  },
  subscriptions: {},
  effects: {
    * ketmon({payload}, {call, put, select}) {
      const res = yield call(userMe);
      if (res.success){
        return res;
      }
    },
    * signIn({payload}, {call, put, select}) {
      const res = yield call(signIn, payload);
      if (res.success) {
        localStorage.setItem(STORAGE_NAME, TOKEN_NAME + " " + res.tokenBody);
        router.push('/cabinet')
      }else{
        alert("Неверный логин или пароль, повторите или пройдите регистрацию!")
      }
    },


    * signUp({payload}, {call, put, select}) {
      const res = yield call(signUp, payload);
      if (res.success) {
        localStorage.setItem(STORAGE_NAME, res.tokenType + " " + res.tokenBody);
        router.push('/cabinet')
      } else {
        alert(res.message)
      }

  }
  },
  reducers: {
    updateState(state, {payload}) {
      return {
        ...state,
        ...payload
      }
    }
  }
})
