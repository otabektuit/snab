import React, {Component} from 'react';
import {connect} from "react-redux";
import {AvField, AvForm} from "availity-reactstrap-validation";
import image from '../ss2.jpg'
import Footer from "../../component/Footer";
import Loader from "../../component/Loader";
import router from "umi/router";
import {FormattedMessage, getLocale} from 'umi-plugin-locale';

@connect(({app}) => ({app}))
class Products extends Component {
  componentDidMount() {
    const {dispatch} = this.props;
    dispatch({
      type: 'app/getBrands'
    });
    dispatch({
      type: 'app/getProducts'
    });
    dispatch({
      type: 'app/getModelsOpen'
    });
  }

  render() {

    const {app, dispatch} = this.props;
    const {products, modelsOpen, brands} = app;
    const token = localStorage.getItem;

    const functionFilterByProduct = () => {
      document.getElementById("filterByProduct").classList.toggle("d-none");
    };

    const functionFilterByBrand = () => {
      document.getElementById("filterByBrand").classList.toggle("d-none");
    };

    const filterModelByProduct = (e) => {
      if (e.target.value === 'Tanlang') {
        dispatch({
          type: 'app/getModelsOpen'
        })
      } else {
        dispatch({
          type: 'app/filterModelByProduct',
          payload: {
            productId: e.target.value
          }
        })
      }
    };

    const filterModelByBrand = (e) => {
      if (e.target.value === 'Tanlang') {
        dispatch({
          type: 'app/getModelsOpen'
        })
      } else {
        dispatch({
          type: 'app/filterModelByBrand',
          payload: {
            brandId: e.target.value
          }
        })
      }
    };

    const order=(item)=>{
      if (token){

        dispatch({
          type:"app/updateState",
          payload:{
            currentOrder:item
          }
        })
        router.push("/order/add")
      }else {
        router.push("/auth/login")
      }
    };

    const findModel=()=>{
      dispatch({
        type:'app/findModelByName',
        payload:{
          modelName:document.getElementById("searchModel").value
        }
      })
    };



    return (

      <div>
        <div className="container">
          <div className="row">
            <div className="col-md-3">
              <div className="jumbotron pb-2 pt-3 px-4 ">
                <h4 className="pb-3">
                  <FormattedMessage id="FILTER"/>
                </h4>
                <button className="btn btn-outline-secondary w-100" onClick={() => functionFilterByProduct()}>
                  <FormattedMessage id="FILTERBYPRODUCT"/>
                </button>
                <AvForm className="d-none" id="filterByProduct">
                  <AvField type="select" placeholder="nameEn ni kiriting" className="form-control"
                           onChange={filterModelByProduct} name="product">
                    <option value="Tanlang">...</option>
                    {
                      products.length > 0 ?
                        products.map(item =>
                          <option value={item.id}>{item.nameRu}</option>
                        ) :
                        <option value="nothing">...</option>
                    }
                  </AvField>
                </AvForm>
                <button className="btn btn-outline-secondary w-100 mt-2" onClick={() => functionFilterByBrand()}><FormattedMessage id="FILTERBYBRAND"/></button>
                <AvForm className="d-none" id="filterByBrand">
                  <AvField type="select" placeholder="nameEn ni kiriting" className="form-control"
                           onChange={filterModelByBrand} name="product">
                    <option value="Tanlang">...</option>
                    {
                      brands.length > 0 ?
                        brands.map(item =>
                          <option value={item.id}>{item.name}</option>
                        ) :
                        <option value="nothing">...</option>
                    }
                  </AvField>
                </AvForm>
                <AvForm className="mt-3 input-group">
                  <AvField type="search" name="modelName" className="form-control" id="searchModel" />
                  <div className="input-group-append">
                      <button className="btn btn-outline-info input-group-text px-1" onClick={()=>findModel()} style={{fontSize:'0.75rem',height:'37.8px'}}><FormattedMessage id="SEARCH"/></button>
                  </div>
                </AvForm>
              </div>
            </div>
            <div className="col-md-9">
              <div className="main-products">
                {
                  modelsOpen.length > 0 ?
                    modelsOpen.map((item, i) =>
                      <div key={i} className="main-products-item"
                           style={{backgroundImage: 'url(/api/attach/' + item.photosId[0] + ')'}}>
                        <h2 className="main-products-title">
                          {item.name}
                        </h2>
                        <div className="main-products-order ">
                          {
                            token ?
                              <button className="btn" onClick={()=>order(item)}>
                                <span className="icon icon-cart"></span>
                              </button>
                              :
                              <a href="/auth/login" className="btn btn-info "><span className="icon icon-cart"></span></a>
                          }
                        </div>
                      </div>
                    ) :
                    <div><h4><FormattedMessage id="NOPRODUCTS"/></h4></div>
                }
              </div>
            </div>
          </div>
        </div>
        <Footer/>
      </div>
    );
  }
}

Products.propTypes = {};

export default Products;
