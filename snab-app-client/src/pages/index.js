import React, {Component} from 'react';
import {Link} from "umi";
import "bootstrap/dist/js/bootstrap"
import "bootstrap/dist/css/bootstrap.min.css"
import "jquery/dist/jquery.min"
import img1 from "./smm-post-1.gif"
import img2 from "./ss2.jpg"
import {Parallax} from "react-parallax";
import Socials from "../component/Socials";
import '../global.css'
import ReactVivus from 'react-vivus';
import deal from '../style/img/deal.svg'
import verify from '../style/img/verified.svg'
import rocket from '../style/img/rocket.svg'
import money from '../style/img/hand.svg'
import Footer from "../component/Footer";
import Dashboard from "../component/Dashboard";
import {connect} from "react-redux";
import Products from "../component/Products";
import {FormattedMessage} from "umi-plugin-locale";

@connect(({app}) => ({app}))
class Index extends Component {

  render() {
    const token = localStorage.getItem;
    return (
      <div className="bg-light landing-back">
        <div>
          <Socials/>
          <div className="container pt-4">
            <div className="row">
              <div className="col-md-4  text-right text-upper">
                <h2 className="pr-2">
                  iMS
                </h2>
                <h5 className="font-weight-lighter">
                  <FormattedMessage id="ABOUTUS"/>
                </h5>
                {
                  token ?
                    <Link to="/products"
                          className="btn btn-outline-secondary float-right text-decoration-none text-secondary"><FormattedMessage id="ORDER"/></Link>
                    :
                    <a href="/auth/register"
                       className="text-decoration-none text-secondary btn btn-outline-secondary float-right">
                      <FormattedMessage id="ORDER"/>
                    </a>
                }
              </div>
            </div>
          </div>
          <Parallax strength={200} blur={3} className="landing-back-half d-sm-none d-md-inline-block d-none"
                    bgImage={require('../style/img/back1.jpg')}>
            <div>
              <div className="position-relative mt-4">
                <img src={img1} className="img-fluid shadow content-image1" alt=""/>
                <img src={img2} className="img-fluid content-image2" alt=""/>
              </div>
            </div>
          </Parallax>
        </div>
        <div className="features bg-light  mt-4 py-4">
          <div className="container">
            <div className="row mt-5">
              <div className="col-md-12">
                <h1 className="main-text  text-secondary">
                  <FormattedMessage id="OURSERVICES"/>
                </h1>
              </div>
            </div>
            <div className="row my-4">
              <div className="col-md-3 col-sm-4 col-6">
                <div className="feature">
                  {/*<span className="feature-icon feature-icon-reliable"></span>*/}
                  <ReactVivus
                    id="foo"
                    option={{
                      file: deal,
                      animTimingFunction: 'EASE',
                      type: 'sync',
                      duration: '300'
                    }}
                    className="feature-icon "
                  />
                  <h3 className="feature-text ">
                    <FormattedMessage id="SAFEDEAL"/>
                  </h3>
                </div>
              </div>
              <div className="col-md-3 col-sm-4 col-6">
                <div className="feature pl-md-5 pl-lg-5 pl-sm-4 pl-4">
                  {/*<span className="feature-icon feature-icon-reliable"></span>*/}
                  <ReactVivus
                    id="foo1"
                    option={{
                      file: verify,
                      animTimingFunction: 'EASE',
                      type: 'sync',
                      duration: '300'
                    }}
                    className="feature-icon pl-md-4 pl-sm-0 pl-0"
                  />
                  <h3 className="feature-text pl-0 pr-4">
                    <FormattedMessage id="SAFEDELIVERY"/>
                  </h3>
                </div>
              </div>
              <div className="col-md-3 col-sm-4 col-6">
                <div className="feature pl-md-4 pl-sm-0 pl-0">
                  {/*<span className="feature-icon feature-icon-reliable"></span>*/}
                  <ReactVivus
                    id="foo2"
                    option={{
                      file: rocket,
                      animTimingFunction: 'EASE',
                      type: 'sync',
                      duration: '300'
                    }}
                    className="feature-icon pl-md-4 pl-sm-4 pl-4 "
                  />
                  <h3 className="feature-text">
                    <FormattedMessage id="FASTESTORDER"/>
                  </h3>
                </div>
              </div>
              <div className="col-md-3 col-sm-4 col-6">
                <div className="feature pl-md-4 pl-sm-0 pl-0 ">
                  {/*<span className="feature-icon feature-icon-reliable"></span>*/}
                  <ReactVivus
                    id="foo3"
                    option={{
                      file: money,
                      animTimingFunction: 'EASE',
                      type: 'sync',
                      duration: '300'
                    }}
                    className="feature-icon pl-md-4 pl-sm-2 pl-2"
                  />
                  <h3 className="feature-text">
                    <FormattedMessage id="COMFORTPRICES"/>
                  </h3>
                </div>
              </div>
            </div>
          </div>

        </div>
        <div className="products-landing bg-light">
          <div className="container">
            <div className="row py-5">
              <div className="col-md-12">
                <h1 className="main-text  text-secondary">
                  <FormattedMessage id="CLIENTORDERS"/>
                </h1>
              </div>
            </div>
            <Products/>
          </div>
        </div>
        <Dashboard/>
        <div className="partners bg-light pb-5">
          <div className="container py-4">
            <div className="row py-5">
              <div className="col-md-12">
                <h1 className="main-text  text-secondary">
                  <FormattedMessage id="PARTNERS"/>
                </h1>
              </div>
            </div>
            <div className="row">
              <div className="col-md-2 col-sm-3 col-4">
                <span className="partner-icon partner-icon-telegram">
                </span>
              </div>
              <div className="col-md-2 col-sm-3 col-4">
                <span className="partner-icon partner-icon-telegram">
                </span>
              </div>
              <div className="col-md-2 col-sm-3 col-4">
                <span className="partner-icon partner-icon-telegram">
                </span>
              </div>
              <div className="col-md-2 col-sm-3 col-4">
                <span className="partner-icon partner-icon-telegram">
                </span>
              </div>
              <div className="col-md-2 col-sm-3 col-4">
                <span className="partner-icon partner-icon-telegram">
                </span>
              </div>
              <div className="col-md-2 col-sm-3 col-4">
                <span className="partner-icon partner-icon-telegram">
                </span>
              </div>

            </div>
          </div>
        </div>
        <Footer/>
      </div>
    );
  }
}

Index.propTypes = {};

export default Index;
