import React, {Component} from 'react';
import {connect} from "react-redux";
import SideBarAdmin from "../../component/SideBarAdmin";
import DashboardCabinet from "../../component/DashboardCabinet";
import Products from "../../component/Products";
import router from "umi/router";
import {FormattedMessage} from "umi-plugin-locale";

@connect(({app}) => ({app}))
class Cabinet extends Component {


  render() {
    const {app} = this.props;
    const {currentUser} = app;
    return (
      <div>
        {
          currentUser.roles ?
            currentUser.roles.filter(i => i.roleName === 'ROLE_ADMIN' || i.roleName === 'ROLE_USER').length > 1 ?
              <SideBarAdmin/>
              :
              <div/>
            :
            <div/>
        }
        <div className="container">
          <div className="row">
            <div/>
            <div className="col-md-10 my-3 offset-md-1">
              <h3 className='text-center mb-3 text-info'>
                <FormattedMessage id="WELCOME"/>
                <i
                className='text-secondary'>{currentUser.firstName}</i></h3>
            </div>
            <div className="col-md-3 offset-md-1">
              <span>
                <FormattedMessage id="USERFULLNAME"/>
              </span>
              <h4>{currentUser.firstName} {currentUser.lastName}</h4>
            </div>
            <div className="col-md-3 offset-md-1">
              <span>
                <FormattedMessage id="USERPHONENUMBER"/>
              </span>
              <h4>{currentUser.phoneNumber}</h4>
            </div>
            <div className="col-md-3">
              <span>Email</span>
              <h4>{currentUser.email}</h4>
            </div>
          </div>
          <div className="row">
            <div className="col-md-10 offset-md-1">
              <div className="row my-4 text-center">
                <DashboardCabinet/>
                <div className="col-md-9">
                  <div>
                    <h3 className='text-secondary'>
                      <FormattedMessage id="PRODUCTS"/>
                    </h3>
                  </div>
                  <Products/>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

Cabinet.propTypes = {};

export default Cabinet;
