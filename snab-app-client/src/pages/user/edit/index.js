import React, {Component} from 'react';
import {Card, Col, Container, Row} from "reactstrap";
import {AvField, AvForm} from "availity-reactstrap-validation";
import {connect} from "react-redux";
import {formatMessage, FormattedMessage} from "umi-plugin-locale";

@connect(({app, auth, user}) => ({app, auth, user}))
class Index extends Component {
  componentDidMount() {
    const {dispatch} = this.props;
    dispatch({
      type: 'app/userMe'
    })
  }

  render() {

    const {dispatch, app, auth, user} = this.props;
    const {currentUser} = app;
    const {email, firstName, lastName, password} = user;


    const changeState = (e) => {
      dispatch({
        type: 'user/updateState',
        payload: {
          [e.target.name]: e.target.value
        }
      })
    };



    const editUser = () => {
      let prePassword =  document.getElementById("prePassword").value;
      if (firstName || lastName || email || password) {
        if (password) {
          if (password === prePassword) {
            if (!document.getElementById("error").classList.contains('d-none')){
              document.getElementById("error").classList.add('d-none');
            }
            document.getElementById("success").classList.toggle('d-none');
            dispatch({
              type: 'user/editUser',
              payload: {
                firstName, lastName, email, password
              }
            })
          } else {
            if (!document.getElementById("success").classList.contains('d-none')){
            document.getElementById("success").classList.add('d-none');
          }
            document.getElementById("error").classList.toggle('d-none');
          }
          return
        }
        dispatch({
          type: 'user/editUser',
          payload: {
            firstName, lastName, email, password
          }
        })
      }
    };



    return (
      <div className="register-page mt-4">
        <Container>
          <Row>
            <Col md={6} className="offset-md-3">
              <Card className="border-0">
                <Row>
                  <Col md={10} className="offset-md-1 card-body text-center bg-light jumbotron shadow"
                       style={{borderRadius: '20px'}}>
                    <h1 className="text-uppercase">
                      <FormattedMessage id="EDIT"/>
                    </h1>
                    <AvForm className="register">
                      <AvField onChange={changeState} defaultValue={currentUser.firstName} name="firstName" id="firstName"
                               placeholder={formatMessage({id:'USERNAME'})}
                               required/>
                      <AvField onChange={changeState} defaultValue={currentUser.lastName} name="lastName" id="lastName"
                               placeholder={formatMessage({id:'USERSURNAME'})}
                               required/>
                      <AvField onChange={changeState} defaultValue={currentUser.email} name="email" placeholder="Email" id="email"
                               required/>
                      <AvField onChange={changeState} name="prepassword" type="password" id="prePassword"
                               placeholder={formatMessage({id:'PWDNEW'})}
                               required/>
                      <AvField onChange={changeState} name="password" type="password"
                               placeholder={formatMessage({id:'PWDCONFIRM'})}
                               required/>
                      <button className="btn btn-success btn-block mt-2" onClick={() => editUser()}>
                        <FormattedMessage id="EDIT"/>
                      </button>
                      <span className="d-none text-danger" id="error">
                        <FormattedMessage id="PWDNOTMATCH"/>
                      </span>
                      <span className="d-none text-success" id="success">
                        <FormattedMessage id="PWDTMATCH"/>
                      </span>
                    </AvForm>
                  </Col>
                </Row>
              </Card>
            </Col>
          </Row>
        </Container>
      </div>
    );
  }
}

Index.propTypes = {};

export default Index;
