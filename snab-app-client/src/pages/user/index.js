import React, {Component} from 'react';
import {connect} from "react-redux";
import {Modal, ModalHeader} from "reactstrap";
import {FormattedMessage} from "umi-plugin-locale";

@connect(({user, app}) => ({user, app}))
class User extends Component {
  componentDidMount() {
    const {dispatch} = this.props;
    dispatch({
      type: 'user/getUsers'
    });
  }

  render() {

    const {dispatch, user, app} = this.props;
    const {products} = app;
    const {isShowModal, deleteModal, deletedId, currentUser, users} = user;

    const openModal = (item) => {
      dispatch({
        type: 'user/updateState',
        payload: {
          isShowModal: !isShowModal,
          currentUser: item
        }
      })
    };


    const save = (e, v) => {
      if (currentUser.id) {
        dispatch({
          type: 'user/editUser',
          payload: {
            path: currentUser.id,
            ...v
          }
        })
      } else {
        dispatch({
          type: 'user/addUser',
          payload: {
            ...v
          }
        })
      }
    };


    const openModalDelete = (id) => {
      dispatch({
        type: 'user/updateState',
        payload: {
          deletedId: id,
          deleteModal: !deleteModal
        }
      })
    };

    const deleteUser = () => {
      dispatch({
        type: 'user/deleteUser',
        payload: {
          id: deletedId,
          deleteModal: false
        }
      })
    };


    return (

        <div>

          <div className="container d-inline-block">
            <div className="row justify-content-center">
              <div className="col-md-8 offset-md-2">
                <h1>
                  <FormattedMessage id="CLIENTS"/>
                </h1>
                <br/>
                {users.length > 0 ?
                  <table border="1" className='table table-bordered table-striped'>
                    <thead>
                    <tr>
                      <th>#</th>
                      <th>
                        <FormattedMessage id="USERFULLNAME"/>
                      </th>
                      <th>
                        <FormattedMessage id="USERPHONENUMBER"/>
                      </th>
                      <th>Email</th>
                      <th>
                        <FormattedMessage id="CREATEDAT"/>
                      </th>
                      <th>Role</th>
                      <th>
                        <FormattedMessage id="DELETEUSER"/>
                      </th>
                    </tr>
                    </thead>
                    <tbody>
                    {users.map((item, i) =>
                      <tr key={item.id}>
                        <td>{i + 1}</td>
                        <td>{item.firstName} {item.lastName}</td>
                        <td>{item.phoneNumber}</td>
                        <td>{item.email}</td>
                        <td>{item.createdAt}</td>
                        <td>
                          {item.roles.map(k =>
                            <p key={k.id}>{k.roleName}</p>
                          )}
                        </td>
                        <td>
                          {item.roles.length > 1 ?
                            'Super Admin' :
                            <button onClick={() => openModalDelete(item.id)} className='btn btn-danger'>To
                              Trash</button>
                          }
                        </td>
                      </tr>
                    )}
                    </tbody>
                  </table> :
                  <h1>User mavjud emas</h1>
                }

                <Modal isOpen={deleteModal} toggle={openModalDelete}>
                  <ModalHeader>
                    <h2>Delete User?</h2>
                  </ModalHeader>
                  <button onClick={deleteUser} className='btn btn-danger'>Delete</button>
                  <button onClick={openModalDelete} className='btn btn-secondary'>Back</button>
                </Modal>

              </div>
            </div>
          </div>
        </div>

    );
  }
}

User.propTypes = {};

export default User;
