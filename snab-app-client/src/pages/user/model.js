import api from 'api'
import {toast} from "react-toastify";
import {router} from "umi";

const {getUsers, addUser, editUser, deleteUser} = api;

export default {
  namespace: 'user',
  state: {
    currentUser: {},
    isShowModal: false,
    deleteModal: false,
    deletedId: '',
    users: [],
    password: '',
    email: '',
    lastName: '',
    firstName: '',

  },
  subscriptions: {},
  effects: {
    * deleteUser({payload}, {call, put, select}) {
      const res = yield call(deleteUser, payload);
      if (res.success) {
        yield put({
          type: 'updateState',
          payload: {
            deleteModal: false,
            deletedId: ''
          }
        });
        yield put({
          type: 'getUsers'
        });
        toast.success(res.message);
      } else {
        toast.error(res.message);
      }
    },
    * getUsers({payload}, {call, put, select}) {
      const res = yield call(getUsers);
      if (res.success) {
        yield put({
          type: 'updateState',
          payload: {
            users: res.object
          }
        })
      }
    },
    * editUser({payload}, {call, put, select}) {
      const res = yield call(editUser, payload);
      if (res.success) {
        toast.success(res.message);
        router.push('/cabinet');
      }
      toast.success(res.message);
    }
  },
  reducers: {
    updateState(state, {payload}) {
      return {
        ...state,
        ...payload
      }
    }
  }
}
