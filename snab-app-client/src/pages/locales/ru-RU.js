export default {
  LANG: 'RU',
  MAIN: 'Главная',
  CABINET: 'Кабинет',
  ORDERS: 'Мои заказы',
  PRODUCTS: 'Продукты',
  FILTERBYPRODUCT: 'Фильтр по продуктам',
  FILTERBYBRAND: 'Фильтр по бренду',
  CHOOSEPRODUCT: 'Выбрать продукт',
  SEARCH: 'Искать',
  CHOOSEBRAND: 'Выбрать бренд',
  ABOUT: 'Про Нас',
  LOGIN: 'Войти',
  CLIENTS: 'Клиенты',
  ABOUTUS: 'Мы аутсорсинговая компания которая занимается импорт снабжением из любых стран в Узбекистан, если вашему предприятию нужно периодически закупать товары из за границы но нет потребности нанимать постоянного сотрудника то мы идельное решение для вас. Мы предоставляем такие услуги как нахождение поставщиков и товаров за границей, полное содействие в переговорах，транспортные услуги и оформление документов под ключь',
  ORDER: 'Заказать',
  OURSERVICES: 'Наши Услуги',
  SAFEDEAL: 'Безопасная сделка',
  SAFEDELIVERY: 'Безопасная Доставка',
  FASTESTORDER: 'Мгновенный Заказ',
  COMFORTPRICES: 'Недорогие Услуги',
  CLIENTORDERS: 'Заказы Наших Клиентов',
  NORPRODUCTS: 'Нет продуктов',
  NEW: 'Новый',
  NEWS: 'Новости',
  PARTNERS: 'Партнеры',
  ADDRESS: 'Адрес',
  LOCATION: 'Узбекитсан , город Ташкент',
  HOMENUMBER: 'Офис : +998998154828',
  PHONENUMBER: 'Допольнтельный тел-номер : +998998154828',
  SOCIALS: 'Социальные Сети',
  SIGNIN: 'Войти',
  SIGNUP: 'Регистрация',
  HAVENOTACCOUNT: 'Еще нет Аккаунта ? ',
  HAVEACCOUNT: 'Нет Аккаунта ? ',
  READY: 'Готов',
  CODESENT: 'Код прислан',
  SUBMIT: 'Подтвердить',
  MODEL: 'Модель',
  BRAND: 'Бренд',
  PRODUCTNAME: 'Имя продукта',
  OPTIONS: 'Характеристики',
  PRICE: 'Цена',
  PWDNOTMATCH: 'Пароль не совпадает',
  PWDMATCH: 'Пароль совпадает',
  PWDCONFIRM: 'Подтвердите пароль',
  PWDNEW: 'Введите новый пароль',
  EDIT: 'Изменить',
  USERNAME: 'Имя пользователья',
  USERSURNAME: 'Фамилия пользователья',
  USERFULLNAME: 'Полная имя',
  USERPHONENUMBER: 'Номер телефона',
  WELCOME: 'Добро пожаловать ',
  FILTER: 'Фильтр',
  CREATEDAT:'Дата первого входа',
  DELETEUSER:'Удалить клиента',
  USERORDERS:'Ваши заказы',
  NOORDER:'Заказов еще нет ',
  DELETE:'Удалить',

}
