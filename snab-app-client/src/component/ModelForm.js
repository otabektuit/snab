import React, {Component} from 'react';
import {AvField, AvForm} from "availity-reactstrap-validation";
import PropTypes from "prop-types";

class ModelForm extends Component {

  render() {
    const {
      add, edit, brands, uploadPhotos, products, model
    } = this.props;
    return (
      <div>
        <div className="container">
          <h1>{model.id ? `Edit model` : `Add Model`}</h1>
          <AvForm onValidSubmit={model.id ? edit : add}>
            <div className="row">
              <div className="col-md-4">
                <AvField name="name" value={model ? model.name : ''} placeholder="Enter model name"/>
              </div>
              <div className="col-md-4">
                <AvField name="price" value={model ? model.price : ''}
                         placeholder="Enter model price"/>
              </div>
              <div className="col-md-4">
                <AvField name="option" placeholder="Enter model option" value={model ? model.option : ''}/>
              </div>
            </div>
            <div className="row">
              <div className="col-md-4">
                <AvField name="productId" type="select"
                         value={model ? model.productId : ''}>
                  <option>Select Product</option>
                  {products.map(item =>
                    <option key={item.id} value={item.id}>{item.nameUz}</option>
                  )}
                </AvField>
              </div>
              <div className="col-md-4">
                <AvField name="brandId" type="select" value={model ? model.brandId : ''}>
                  <option>Select Brand</option>
                  {brands.map(item =>
                    <option key={item.id} value={item.id}>{item.name}</option>
                  )}
                </AvField>
              </div>
            </div>
            <div className="row">
              <div className="col-md-12"><h3>Upload model photos</h3></div>
              <div className="col-md-4">
                <div className="custom-file">
                  <AvField name="photosId" className="custom-file-input" onChange={uploadPhotos} type="file"/>
                  <label className="custom-file-label" htmlFor="customFile">Choose file</label>
                </div>
              </div>
            </div>
            <button type="submit" className="btn btn-success text-right mt-3">Saqlash</button>
          </AvForm>

          {model == null ? <h1>Rasm mavjud emas</h1> :
            <div className="row mt-3">
              <div className="col-md-6 ">
                {model.photosId == null ? '' : model.photosId.map((item, i) =>
                  <div className="mt-3">
                    <img key={item.id} width="300px" height="300px" className="img-fluid" alt='Bu rasm'
                         src={`/api/attach/${item}`}/>
                  </div>
                )
                }
              </div>
            </div>
          }
        </div>
      </div>

    );
  }
}

ModelForm.propTypes = {
  save: PropTypes.func,
};

export default ModelForm;
