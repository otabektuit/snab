import React, {Component} from 'react';
import {Card, CardImg} from "reactstrap";
import InfiniteCarousel from "react-leaf-carousel";
import {FormattedMessage, getLocale} from "umi-plugin-locale";

class OrderInfoForm extends Component {
  render() {
    const {currentOrder, add} = this.props;

    let lang = getLocale();

    let productLang = (language) => {
      if (language === 'uz-UZ') return currentOrder.productNameUz;
      if (language === 'ru-RU') return currentOrder.productNameRu;
      if (language === 'en-US') return currentOrder.productNameEn;
    };

    return (
      <div>
        <div className="container pt-4">
          <div className="row">
            <div className="col-md-4  text-right text-upper">
              <h2 className="pr-2">
                iMS
              </h2>
              <h5 className="font-weight-lighter">
                <p><FormattedMessage id="MODEL"/>: {currentOrder.name}</p>
                <p><FormattedMessage id="BRAND"/>: {currentOrder.brandName}</p>
                <p><FormattedMessage id="PRODUCTNAME"/>: {
                  productLang(getLocale())
                }

                </p>
                <p><FormattedMessage id="OPTIONS"/>: {currentOrder.option}</p>
                <p><FormattedMessage id="PRICE"/>: {currentOrder.price}</p>
              </h5>
            </div>
            <div className="col-md-8">
              <InfiniteCarousel
                breakpoints={[
                  {
                    breakpoint: 500,
                    settings: {
                      slidesToShow: 1,
                      slidesToScroll: 1,
                    },
                  },
                  {
                    breakpoint: 768,
                    settings: {
                      slidesToShow: 2,
                      slidesToScroll: 1,
                    },
                  },
                  {
                    breakpoint: 900,
                    settings: {
                      slidesToShow: 3,
                      slidesToScroll: 1,
                    },
                  }
                ]}
                showSides={true}
                sidesOpacity={.5}
                sideSize={.1}
                slidesToScroll={1}
                slidesToShow={4}
                scrollOnDevice={true}
                autoCycle={true}
                responsive={true}
                incrementalSides={true}
                pauseOnHover={true}

              >
                {
                  currentOrder.photosId.length > 0
                    ?
                    currentOrder.photosId.map((item) =>
                      <div key={item}>
                        <div className="product">
                          <Card>
                            <CardImg top width="100%" key={item} src={`/api/attach/${item}`}
                                     alt="Bu rasm"/>
                          </Card>
                        </div>
                      </div>
                    ) :
                    <div>
                      <h1><FormattedMessage id="NOPRODUCTS"/></h1>
                    </div>
                }
              </InfiniteCarousel>
            </div>
          </div>
          <div className="row pt-5">
            <div className="col-md-6 offset-3">
              <button type="button" className="btn btn-success" onClick={add}><FormattedMessage id="ORDER"/></button>
            </div>
          </div>
        </div>

      </div>
    );
  }
}

OrderInfoForm.propTypes = {};

export default OrderInfoForm;
