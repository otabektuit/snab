import React, {Component} from 'react';
import {connect} from "react-redux";
import {FormattedMessage, getLocale} from "umi-plugin-locale";

@connect(({app}) => ({app}))
class Dashboard extends Component {
  componentDidMount() {
    const {dispatch} = this.props;
    dispatch({
      type: 'app/getDashboards'
    })
  }

  render() {
    const {app} = this.props;
    const {dashboards} = app;
    const sideDashboards = () => {
      const sideDashboardsList = [];
      const count = 2;
      if (dashboards !== null) {
        for (let i = dashboards.length - 2; i > -1; i--) {
          if (count !== 0) {
            sideDashboardsList.push(dashboards[i]);
          } else {
            break;
          }
        }
      }

      return sideDashboardsList;
    };

    return (
      <div className="bg-light">
        <div className="container">
          <div className="row py-5">
            <div className="col-md-12">
              <h1 className="main-text  text-secondary">
                <FormattedMessage id="NEWS"/>
              </h1>
            </div>
          </div>
          <div className="row">
            {
              dashboards.length > 0
                ?
                <div className="col-md-7 col-sm-10 offset-md-0 offset-sm-1 col-12 ">
                  <div className="main-news shadow"
                       style={{backgroundImage: "url(" + `/api/attach/${dashboards[dashboards.length - 1].attachmentId}` + ")"}}>
                    <h3 className="main-news-title">
                      {
                        getLocale() === 'uz-UZ'
                          ?
                          dashboards[dashboards.length - 1].nameUz
                          :
                          getLocale() === 'ru-RU'
                            ?
                            dashboards[dashboards.length - 1].nameRu
                            :
                            dashboards[dashboards.length - 1].nameEn
                      }
                    </h3>
                    <h5 className="main-news-descrp">
                      {
                        getLocale() === 'uz-UZ'
                          ?
                          dashboards[dashboards.length - 1].descrpUz
                          :
                          getLocale() === 'ru-RU'
                            ?
                            dashboards[dashboards.length - 1].descrpRu
                            :
                            dashboards[dashboards.length - 1].descrpEn
                      }
                    </h5>
                    <p className="main-news-about">
                      {
                        getLocale() === 'uz-UZ'
                          ?
                          dashboards[dashboards.length - 1].aboutUz
                          :
                          getLocale() === 'ru-RU'
                            ?
                            dashboards[dashboards.length - 1].aboutRu
                            :
                            dashboards[dashboards.length - 1].aboutEn
                      }
                    </p>
                    <span className="main-news-data">
                      {dashboards[dashboards.length - 1].createdAt.toString().substring(11, 16)}&nbsp;
                      {dashboards[dashboards.length - 1].createdAt.toString().substring(0, 10)}
                    </span>
                  </div>
                </div>
                :
                <div>No more News</div>
            }
            <div className="col-md-5 px-3 col-sm-12 col-12">
              {
                sideDashboards().length > 0
                  ?
                  sideDashboards().map((item, i) =>
                    <div className="side-news row " key={i}>
                      <div className="col-md-5 side-news-img w-100 col-sm-5 col-5">
                        <img src={`/api/attach/${item.attachmentId}`} className="img-fluid w-100 shadow" alt=""/>
                      </div>
                      <div className="col-md-7 col-sm-7 col-7">
                        <h4 className="side-news-title">
                          {
                            getLocale() === 'uz-UZ'
                              ?
                              item.nameUz
                              :
                              getLocale() === 'ru-RU'
                                ?
                                item.nameRu
                                :
                                item.nameEn
                          }
                        </h4>
                        <h6 className="side-news-descrp">
                          {
                            getLocale() === 'uz-UZ'
                            ?
                            item.descrpUz
                            :
                            getLocale() === 'ru-RU'
                              ?
                              item.descrpRu
                              :
                              item.descrpEn
                          }
                        </h6>
                        <p className="side-news-about">
                          {
                            getLocale()==='uz-UZ'
                              ?
                              item.aboutUz
                              :
                              getLocale()==='ru-RU'
                                ?
                                item.aboutRu
                                :
                                item.aboutEn
                          }
                        </p>
                        <span className="side-news-data">
                            {dashboards[dashboards.length - 1].createdAt.toString().substring(11, 16)}&nbsp;
                          {dashboards[dashboards.length - 1].createdAt.toString().substring(0, 10)}
                          </span>
                      </div>
                    </div>
                  ) : <div>No more News</div>
              }
            </div>
          </div>
        </div>
      </div>
    );
  }
}

Dashboard.propTypes = {};

export default Dashboard;
