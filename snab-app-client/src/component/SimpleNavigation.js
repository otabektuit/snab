import React, {Component} from 'react';
import {Link} from "react-router-dom";
import {FormattedMessage} from "umi-plugin-locale";
import Language from "./Language";

class SimpleNavigation extends Component {
  render() {

    const toggleNav = () => {
      document.getElementById("navbarSupportedContent").classList.toggle("show");
    };

    const buttonChange=()=>{
      if(document.getElementById('navButton').classList.contains('navbar-toggler-icon')){
        document.getElementById('navButton').classList.add('icon-close');
        document.getElementById('navButton').classList.remove('navbar-toggler-icon');
      }
      else
      {
        document.getElementById('navButton').classList.remove('icon-close');
        document.getElementById('navButton').classList.add('navbar-toggler-icon');
      }
    };


    return (

      <nav className=" navbar navbar-expand-lg navbar-light"
           style={{backgroundColor: 'rgba(255,255,255,0.7)', width: '100%'}}>

        <div className="container">
          <Link className="navbar-brand" to='/'>
            Logo
          </Link>
          <button className="navbar-toggler border-0" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                  aria-controls="navbarSupportedContent"
                  onClick={buttonChange}
                  aria-expanded="false" aria-label="Toggle navigation">
            <span className="navbar-toggler-icon" id="navButton"></span>
          </button>

          <div className="collapse navbar-collapse"
               id="navbarSupportedContent">
            <ul className="navbar-nav mr-auto  float-right ">
              <li className="nav-item">
                <Link className="nav-link" to="/"><FormattedMessage id="MAIN"/></Link>
              </li>
              <li className="nav-item">
                <Link className="nav-link" to="/products"><FormattedMessage id="PRODUCTS"/></Link>
              </li>
              <li className="nav-item">
                <Link className="nav-link" to="/"><FormattedMessage id="ABOUT"/></Link>
              </li>
              <li className="nav-item">
                <Link className="nav-link" to="/auth/login"><FormattedMessage id="LOGIN"/></Link>
              </li>
            </ul>

            <Language/>
            <span className='nav-link text-center text-secondary d-sm-none d-none d-md-inline'> +998998154828</span>
          </div>

        </div>
      </nav>
    );
  }
}

SimpleNavigation.propTypes = {};

export default SimpleNavigation;
