import React, {Component} from 'react';
import {Link} from "react-router-dom";

class SideBarAdmin extends Component {
  render() {

    const openNav=()=>{
      document.getElementById("navigation").classList.toggle("unblock")
    }

    return (
      <div>
        <button className="d-lg-none d-md-none d-sm-block d-block sidebar-block w-100 btn btn-outline-secondary" onClick={openNav} id="navigationBtn">
          Navigation
        </button>
        <div className="">
          <div className='sidebar-menu position-fixed bg-white h-100 d-lg-block' id="navigation">
            <div className='w-100'>
              <Link className='btn rounded-0 w-100 btn-outline-secondary border-0'
                    to="/catalog/country">Country</Link><br/>
              <Link className='btn rounded-0 w-100 btn-outline-secondary border-0' to="/catalog/region">Region</Link><br/>
              <Link className='btn rounded-0 w-100 btn-outline-secondary border-0'
                    to="/catalog/category">Category</Link><br/>
              <Link className='btn rounded-0 w-100 btn-outline-secondary border-0'
                    to="/catalog/product">Product</Link><br/>
              <Link className='btn rounded-0 w-100 btn-outline-secondary border-0' to="/catalog/brand">Brand</Link><br/>
              <Link className='btn rounded-0 w-100 btn-outline-secondary border-0' to="/catalog/model">Model</Link><br/>
              <Link className='btn rounded-0 w-100 btn-outline-secondary border-0'
                    to="/catalog/dashboard">Dashboard</Link><br/>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

SideBarAdmin.propTypes = {};

export default SideBarAdmin;
