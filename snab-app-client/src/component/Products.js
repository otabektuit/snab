import React, {Component} from 'react';
import InfiniteCarousel from "react-leaf-carousel";
import {Card, CardBody, CardFooter, CardImg, CardSubtitle, CardText, CardTitle} from "reactstrap";
import {Link} from "umi";
import {connect} from "react-redux";
import router from "umi/router";
import {FormattedMessage, getLocale} from "umi-plugin-locale";

@connect(({app}) => ({app}))
class Products extends Component {
  componentDidMount() {
    const {dispatch} = this.props;
    dispatch({
      type: 'app/getModelsOpen'
    })
  }

  render() {
    const {app, dispatch} = this.props;
    const {modelsOpen} = app;
    const token = localStorage.getItem;
    const doOrder = (item) => {
      if (token) {
        dispatch({
          type: "app/updateState",
          payload: {
            currentOrder: item
          }
        });
        router.push("/order/add")
      } else {
        router.push("/auth/login")
      }
    };
    return (
      <div>
        <div className="container">
          <div className="row">
            <div className="col-md-12">
              <InfiniteCarousel
                breakpoints={[
                  {
                    breakpoint: 500,
                    settings: {
                      slidesToShow: 1,
                      slidesToScroll: 1,
                    },
                  },
                  {
                    breakpoint: 768,
                    settings: {
                      slidesToShow: 2,
                      slidesToScroll: 1,
                    },
                  },
                  {
                    breakpoint: 900,
                    settings: {
                      slidesToShow: 3,
                      slidesToScroll: 1,
                    },
                  }
                ]}
                showSides={true}
                sidesOpacity={.5}
                sideSize={.1}
                slidesToScroll={1}
                slidesToShow={4}
                scrollOnDevice={true}
                autoCycle={true}
                responsive={true}
                incrementalSides={true}
                pauseOnHover={true}

              >
                {
                  modelsOpen.length > 0
                    ?
                    modelsOpen.map((item, i) =>
                      <div key={item.id}>
                        <div className="product">
                          <Card>
                            <CardImg top width="100%" key={item.photosId[0]} src={`/api/attach/${item.photosId[0]}`}
                                     alt="Bu rasm"/>
                            <CardBody>
                              <CardTitle>
                                <h3 className="d-inline">
                                  {item.name}
                                </h3>
                                <span
                                  className="badge badge-secondary d-inline rounded-6 float-right p-1"><FormattedMessage id="NEW"/></span>
                              </CardTitle>
                              <CardSubtitle>
                                <h5>
                                  {item.brandName} , {
                                  getLocale()==='uz-UZ'
                                    ?
                                    item.productNameUZ
                                    :
                                    getLocale()==='ru-RU'
                                      ?
                                      item.productNameRu
                                      :
                                      item.productNameEn

                                }
                                </h5>
                              </CardSubtitle>
                              <CardText>{item.option}</CardText>
                            </CardBody>
                            <CardFooter>
                              <div>
                                <span>Price:</span>
                                <spand className="float-right">{item.price}</spand>
                              </div>
                              <button className="text-decoration-none btn btn-success float-right px-1 py-0" onClick={() => doOrder(item)}
                                 >
                                <FormattedMessage id="ORDER"/>
                              </button>
                            </CardFooter>
                          </Card>
                        </div>
                      </div>
                    ) :
                    <div>
                      <h1>
                        <FormattedMessage id="NORPRODUCTS"/>
                      </h1>
                    </div>
                }

              </InfiniteCarousel>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

Products.propTypes = {};

export default Products;
