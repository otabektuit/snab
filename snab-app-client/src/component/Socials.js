import React, {Component} from 'react';
import {Link} from "umi";
import {FormattedMessage} from "umi-plugin-locale";

class Socials extends Component {
  render() {
    return (
      <div className="socials d-sm-none d-md-block d-none position-fixed">
        <Link to='#' className="social social-telegram">
        </Link>
        <Link to='#' className="social social-instagram">
        </Link>
        <Link to='#' className="social social-gmail">
        </Link>
        <div className="parent-social-hover">
            <span className="social-hover">
              <FormattedMessage id="SOCIALS"/>
            </span>
        </div>
      </div>

    );
  }
}

Socials.propTypes = {};

export default Socials;
