import React, {Component} from 'react';

class Loader extends Component {
  render() {
    setTimeout(function(){
        if (!document.getElementById("loader").classList.contains("d-none")){
          document.getElementById("loader").classList.add("d-none");
        }else{
          document.getElementById("loader").classList.remove(
            "d-none");
        }
       }, 5300);
    return (
      <div>
        <section className="wrapper" id="loader">
          <div className="spinner">
            <i></i>
            <i></i>
            <i></i>
            <i></i>
            <i></i>
            <i></i>
            <i></i>
          </div>
        </section>
      </div>
    );
  }
}

Loader.propTypes = {};

export default Loader;
