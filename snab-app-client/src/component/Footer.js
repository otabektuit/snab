import React, {Component} from 'react';
import {Link} from "umi";
import {FormattedMessage} from "umi-plugin-locale";

class Footer extends Component {
  render() {
    return (
      <div>
        <footer className="w-100 bg-dark d-block text-white-50 pt-5">
          <div className="container">
            <div className="row">
              <div className="col-md-4">
                <h5 className="text-center">
                  <FormattedMessage id="ADDRESS"/>
                </h5>
                <ul>
                  <li>
                    <FormattedMessage id="LOCATION"/>
                  </li>
                  <li>
                    <FormattedMessage id="HOMENUMBER"/>
                  </li>
                  <li>
                    <FormattedMessage id="PHONENUMBER"/>
                  </li>
                </ul>
              </div>
              <div className="col-md-4">
                <h5 className="text-center">
                  <FormattedMessage id="SOCIALS"/>
                </h5>
                <ul>
                  <li>
                    <Link to="instagram.com/otabeknos1rov" className="text-decoration-none">
                      Instagram
                    </Link>
                  </li>
                  <li>
                    <Link to="instagram.com/otabeknos1rov" className="text-decoration-none">
                      Telegram
                    </Link>
                  </li>
                  <li>
                    <Link to="instagram.com/otabeknos1rov" className="text-decoration-none">
                      Facebook
                    </Link>
                  </li>
                </ul>
              </div>
              <div className="col-md-4">
                <h2>LOGO</h2>
              </div>
            </div>
          </div>
          <hr/>
          <div className="container">
            <div className="row">
              <div className="col-md-6 offset-md-3 text-center">
                <a href="https://instagram.com/otabeknos1rov" className="text-decoration-none text-white-50">
                  <span className="text-center mb-3">
                  Developed by obdev
                </span>
                </a>
              </div>
            </div>
          </div>
        </footer>
      </div>
    );
  }
}

Footer.propTypes = {};

export default Footer;
