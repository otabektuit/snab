import React, {Component} from 'react';
import {Link} from "react-router-dom";
import {STORAGE_NAME} from "../utils/constant";
import router from "umi/router";
import {FormattedMessage} from "umi-plugin-locale";
import Language from "./Language";

class AdminNavigation extends Component {
  render() {
    const toggleNav = () => {
      document.getElementById("navbarSupportedContent").classList.toggle("show");
    };
    const logOut=()=>{
      localStorage.removeItem(STORAGE_NAME);
      router.push('/cabinet');
    };

    const buttonChange=()=>{
      if(document.getElementById('navButton').classList.contains('navbar-toggler-icon')){
        document.getElementById('navButton').classList.add('icon-close');
        document.getElementById('navButton').classList.remove('navbar-toggler-icon');
      }
      else
        {
          document.getElementById('navButton').classList.remove('icon-close');
        document.getElementById('navButton').classList.add('navbar-toggler-icon');
      }
    };

    return (

      <nav className="  navbar navbar-expand-lg navbar-light"
           style={{backgroundColor: 'rgba(255,255,255,0.7)', width: '100%'}}>
        <div className="container">
          <Link className="navbar-brand" to='/'>
            Logo
          </Link>
          <button className="navbar-toggler border-0" type="button" data-toggle="collapse"
                  data-target="#navbarSupportedContent1"
                  aria-controls="navbarSupportedContent1"
                  onClick={buttonChange()}
                  aria-expanded="false" aria-label="Toggle navigation">
            <span className="navbar-toggler-icon" id="navButton"></span>
          </button>

          <div className="collapse navbar-collapse" id="navbarSupportedContent1">
            <ul className="navbar-nav mr-auto  float-right">
              <li className="nav-item">
                <Link className="nav-link" to="/"><FormattedMessage id="MAIN"/></Link>
              </li>
              <li className="nav-item">
                <Link className="nav-link" to="/cabinet"><FormattedMessage id="CABINET"/></Link>
              </li>
              <li className="nav-item">
                <Link className="nav-link" to="/order"><FormattedMessage id="ORDERS"/></Link>
              </li>
              <li className="nav-item">
                <Link className="nav-link" to="/products"><FormattedMessage id="PRODUCTS"/></Link>
              </li>
              <li className="nav-item">
                <Link className="nav-link" to="/user"><FormattedMessage id="CLIENTS"/></Link>
              </li>


            </ul>
            <Language/>
            <div className="nav-item">
              <Link to="user/edit" className="nav-link btn">
                <span className="icon icon-settings"></span>
              </Link>
            </div>
            <div className="nav-item">
              <button className="nav-link btn" onClick={()=>logOut()}>
                <span className="icon icon-logout"></span>
              </button>
            </div>
            <span className='nav-link text-center text-secondary d-sm-none'> +998998154828</span>
          </div>

        </div>
      </nav>
    );
  }
}

AdminNavigation.propTypes = {};

export default AdminNavigation;
