import React, {Component} from 'react';
import {getLocale, setLocale} from 'umi-plugin-locale';
import {Button, Col, Dropdown, Menu, Row, Select} from "antd";


class Language extends Component {
  render() {
    const changeLang = (e) => {
      setLocale(e.toString());
    };
    const {Option} = Select;

    return (
      <div>
            <Select
              onChange={changeLang}
              value={getLocale() === 'uz-UZ' ? 'uz-UZ' : getLocale() === 'ru-RU' ? 'ru-RU' : getLocale() === 'en-US' ? 'en-US' : 'uz-UZ'
              }
              className="browser-default custom-select"
              placeholder="Til"
            >
              <Option value="en-US" key="en-US" className="options">EN</Option>
              <Option value="ru-RU" key="ru-RU" className="options">RU</Option>
              <Option value="uz-UZ" key="uz-UZ" className="options">UZ</Option>
            </Select>

      </div>
    );
  }
}

Language.propTypes = {};

export default Language;
