import React, {Component} from 'react';
import {connect} from "react-redux";
import {FormattedMessage, getLocale} from "umi-plugin-locale";

@connect(({app}) => ({app}))
class DashboardCabinet extends Component {
  componentDidMount() {
    const {dispatch} = this.props;
    dispatch({
      type: 'app/getDashboards'
    })
  }

  render() {
    const {app} = this.props;
    const {dashboards} = app;
    const sideDashboards = () => {
      const sideDashboardsList = [];
      const count = 2;
      if (dashboards !== null) {
        for (let i = dashboards.length - 1; i > -1; i--) {
          if (count !== 0) {
            sideDashboardsList.push(dashboards[i]);
          } else {
            break;
          }
        }
      }

      return sideDashboardsList;
    };

    return (
      <div className="col-md-3">
        <div>
          <h3 className='text-secondary'>
            <FormattedMessage id="NEWS"/>
          </h3>
        </div>
        {
          sideDashboards().length>0?
            sideDashboards().map((item , i)=>
                <div className='text-justify mb-3' key={i}>
                  <h5>
                    <i>
                      {
                        getLocale()==='uz-UZ'
                        ?
                          item.nameUz
                          :
                          getLocale()==='ru-RU'
                            ?
                            item.nameRu
                            :
                            item.nameEn
                      }
                    </i>
                  </h5>
                  <span>
                    {
                      getLocale()==='uz-UZ'
                        ?
                        item.aboutUz
                        :
                        getLocale()==='ru-RU'
                          ?
                          item.aboutRu
                          :
                          item.aboutEn
                    }
          </span>
                </div>
            ):
            <div><h4>No more news</h4></div>
        }

      </div>
    );
  }
}

DashboardCabinet.propTypes = {};

export default DashboardCabinet;
