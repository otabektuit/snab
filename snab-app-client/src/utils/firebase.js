import firebase from 'firebase/app';
import 'firebase/auth';

const prodConfig = {
  apiKey: "AIzaSyD0ss3nEf32guH6YdrnVuDSztiXq0Du1cM",
  authDomain: "app-snab.firebaseapp.com",
  databaseURL: "https://app-snab.firebaseio.com",
  projectId: "app-snab",
  storageBucket: "app-snab.appspot.com",
  messagingSenderId: "276657132860",
  appId: "1:276657132860:web:3d80862d0520a99aa7fa39",
  measurementId: "G-9C0WMH562X"
};

const devConfig = {
  apiKey: "AIzaSyD0ss3nEf32guH6YdrnVuDSztiXq0Du1cM",
  authDomain: "app-snab.firebaseapp.com",
  databaseURL: "https://app-snab.firebaseio.com",
  projectId: "app-snab",
  storageBucket: "app-snab.appspot.com",
  messagingSenderId: "276657132860",
  appId: "1:276657132860:web:3d80862d0520a99aa7fa39",
  measurementId: "G-9C0WMH562X"
};

const config = process.env.NODE_ENV === 'production'
  ? prodConfig
  : devConfig;

if (!firebase.apps.length) {
  firebase.initializeApp(config);
}

const firebaseAuth = firebase.auth();

export {
  firebaseAuth, firebase
};
