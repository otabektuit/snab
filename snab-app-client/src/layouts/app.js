import React, {Component} from 'react';
import {connect} from "react-redux";
import AdminNavigation from "../component/AdminNavigation";
import NavigationClient from "../component/NavigationClient";
import SimpleNavigation from "../component/SimpleNavigation";

@connect(({app}) => ({app}))
class App extends Component {
  render() {
    const {app} = this.props;
    const {currentUser} = app;
    return (
      currentUser.authorities ?
        (currentUser.authorities.filter(i => i.roleName === 'ROLE_ADMIN' || i.roleName==='ROLE_USER').length>1 ?
            <div>
              <AdminNavigation/>
              {this.props.children}
            </div>
            :
            currentUser.authorities.filter(i => i.roleName === 'ROLE_USER').length===1 ?
              <div>
                <NavigationClient/>
                {this.props.children}
              </div>
              :
              <div>
                <SimpleNavigation/>
                {this.props.children}
              </div>
        )
        :
        <div>
          <SimpleNavigation/>
          {this.props.children}
        </div>
    )
  }
}

App.propTypes = {};

export default App;
