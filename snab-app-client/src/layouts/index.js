import {ToastContainer} from 'react-toastify';
import "bootstrap/dist/css/bootstrap.min.css"
import 'react-toastify/dist/ReactToastify.css';
import App from "./app";
import React from "react";
import Loader from "../component/Loader";
import Language from "../component/Language";
import {getLocale} from "umi-plugin-locale";
import {LocaleProvider} from "antd";



function BasicLayout(props) {
  return (
    <div>
      <LocaleProvider locale={getLocale() === 'uz-UZ' ? 'uz-UZ' : getLocale() === 'ru-RU' ? 'ru-RU' : getLocale() === 'en-US' ? 'en-US' : 'uz-UZ'
      }>
      <ToastContainer/>
      <App/>
      {props.children}
      </LocaleProvider>
    </div>
  );
}

export default BasicLayout;
