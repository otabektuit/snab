package uz.obdev.appsnab.entity.enums;

public enum OperationName {
    INSERT, UPDATE, DELETE
}
