package uz.obdev.appsnab.entity.enums;

public enum RoleName {
    ROLE_USER, ROLE_ADMIN
}
