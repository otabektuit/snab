package uz.obdev.appsnab.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;
import uz.obdev.appsnab.entity.template.AbsNameEntity;

import javax.persistence.*;
import java.util.List;


@EqualsAndHashCode(callSuper = true)
@Data
@Entity
public class Country extends AbsNameEntity {
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "country", cascade = CascadeType.ALL)
    private List<Region> regions;
}
