package uz.obdev.appsnab.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import uz.obdev.appsnab.entity.template.AbsEntity;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToOne;


@EqualsAndHashCode(callSuper = true)
@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class Dashboard extends AbsEntity {
    private String nameUz;
    private String nameRu;
    private String nameEn;

    private String descrpUz;
    private String descrpRu;
    private String descrpEn;

    private String aboutUz;
    private String aboutRu;
    private String aboutEn;

    @OneToOne(fetch = FetchType.LAZY)
    private Attachment attachment;
}
