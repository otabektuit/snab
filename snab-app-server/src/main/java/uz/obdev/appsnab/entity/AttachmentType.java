package uz.obdev.appsnab.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import uz.obdev.appsnab.entity.enums.AttachmentTypeEnum;
import uz.obdev.appsnab.entity.template.AbsEntity;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;

@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class AttachmentType extends AbsEntity {

    @Enumerated(value = EnumType.STRING)
    private AttachmentTypeEnum attachmentTypeEnum;

    private int width;

    private int height;

    private long size;

    private String contentTypes;
}
