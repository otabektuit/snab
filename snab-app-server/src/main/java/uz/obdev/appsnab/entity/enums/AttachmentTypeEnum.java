package uz.obdev.appsnab.entity.enums;

public enum AttachmentTypeEnum {
    AVATAR,
    BANNER,
    LOGO
}
