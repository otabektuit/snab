package uz.obdev.appsnab.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import uz.obdev.appsnab.entity.template.AbsEntity;

import javax.persistence.*;
import java.util.List;

@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Model extends AbsEntity {
    private String name;
    @ManyToOne(fetch = FetchType.LAZY)
    private Brand brand;
    @ManyToOne(fetch = FetchType.LAZY)
    private Product product;
    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "model_attach", joinColumns = {@JoinColumn(name = "model_id")}, inverseJoinColumns = {@JoinColumn(name = "attach_id")})
    private List<Attachment> attachments;
    private String option;
    private double price;
    @OneToMany(fetch = FetchType.LAZY,mappedBy = "model",cascade = CascadeType.ALL)
    private List<Order> orders;
}
