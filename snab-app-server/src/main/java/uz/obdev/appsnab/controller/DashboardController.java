package uz.obdev.appsnab.controller;

import com.google.protobuf.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uz.obdev.appsnab.entity.Dashboard;
import uz.obdev.appsnab.payload.ApiResponse;
import uz.obdev.appsnab.payload.ReqDashboard;
import uz.obdev.appsnab.repository.DashboardRepository;
import uz.obdev.appsnab.service.DashboardService;

import java.util.UUID;

@RestController
@RequestMapping("/api/dashboard")
public class DashboardController {
    @Autowired
    DashboardService dashboardService;

    @Autowired
    DashboardRepository dashboardRepository;

    @GetMapping
    private HttpEntity<?> getDashboards() {
        return ResponseEntity.ok(new ApiResponse("Mana",true,dashboardService.getDashboards()));
    }

    @GetMapping("{id}")
    private HttpEntity<?> getDashboard(@PathVariable UUID id) {
        Dashboard dashboard = dashboardRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("get Dashboarsd"));
        return ResponseEntity.ok(dashboardService.getDashboard(dashboard));
    }

    @PostMapping
    private HttpEntity<?> add(@RequestBody ReqDashboard reqDashboard) {
        ApiResponse response = dashboardService.addDashboard(reqDashboard);
        return ResponseEntity.status(response.isSuccess() ? HttpStatus.OK : HttpStatus.CONFLICT).body(response);
    }

    @DeleteMapping("{id}")
    private HttpEntity<?> delete(@PathVariable UUID id) {
        ApiResponse response = dashboardService.deleteDashboard(id);
        return ResponseEntity.status(response.isSuccess() ? HttpStatus.OK : HttpStatus.CONFLICT).body(response);
    }

    @PutMapping("/{id}")
    private HttpEntity<?> edit(@PathVariable UUID id,@RequestBody ReqDashboard reqDashboard) {
        ApiResponse response=dashboardService.editDashboard(id,reqDashboard);
        return ResponseEntity.status(response.isSuccess()?HttpStatus.ACCEPTED:HttpStatus.CONFLICT).body(response);
    }
}
