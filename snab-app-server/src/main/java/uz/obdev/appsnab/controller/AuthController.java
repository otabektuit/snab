package uz.obdev.appsnab.controller;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import uz.obdev.appsnab.entity.User;
import uz.obdev.appsnab.payload.ApiResponse;
import uz.obdev.appsnab.payload.JwtResponse;
import uz.obdev.appsnab.payload.ReqUser;
import uz.obdev.appsnab.repository.UserRepository;
import uz.obdev.appsnab.security.AuthService;
import uz.obdev.appsnab.security.CurrentUser;
import uz.obdev.appsnab.security.JwtTokenProvider;

@Controller
@RequestMapping("/api/auth")
public class AuthController {
    private final
    AuthenticationManager authenticationManager;
    private final
    UserRepository userRepository;
    private final
    JwtTokenProvider jwtTokenProvider;
    private final
    AuthService authService;

    public AuthController(AuthenticationManager authenticationManager, UserRepository userRepository, JwtTokenProvider jwtTokenProvider, AuthService authService) {
        this.authenticationManager = authenticationManager;
        this.userRepository = userRepository;
        this.jwtTokenProvider = jwtTokenProvider;
        this.authService = authService;
    }

    @PostMapping("/login")
    public HttpEntity<?> signIn(@RequestBody ReqUser reqLogin) {
        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(reqLogin.getPhoneNumber(), reqLogin.getPassword()));
        SecurityContextHolder.getContext().setAuthentication(authentication);
        String token = jwtTokenProvider.generateToken(authentication);
        return ResponseEntity.ok(new JwtResponse(jwtTokenProvider.generateToken(authentication)));
    }

    @PostMapping("/register")
    public HttpEntity<?> signUp(@RequestBody ReqUser reqLogin) {
        ApiResponse response = authService.addUser(reqLogin);
        return ResponseEntity.status(response.isSuccess() ? HttpStatus.CREATED : HttpStatus.CONFLICT).body(response);
    }

    @GetMapping("/me")
    public HttpEntity<?> getUser(@CurrentUser User user) {
        if(user!=null){
            return ResponseEntity.ok(new ApiResponse("Mana user", true, user));
        }
        return ResponseEntity.ok(new ApiResponse("User mavjud emas", false));
    }
}
