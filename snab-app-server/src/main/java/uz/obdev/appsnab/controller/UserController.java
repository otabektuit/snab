package uz.obdev.appsnab.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uz.obdev.appsnab.entity.User;
import uz.obdev.appsnab.payload.ApiResponse;
import uz.obdev.appsnab.repository.UserRepository;
import uz.obdev.appsnab.security.CurrentUser;
import uz.obdev.appsnab.service.UserService;

import java.util.UUID;

@RestController
@RequestMapping("/api/user")
public class UserController {
    final
    UserService userService;
    final
    UserRepository userRepository;

    public UserController(UserService userService, UserRepository userRepository) {
        this.userService = userService;
        this.userRepository = userRepository;
    }

    @GetMapping
    public HttpEntity<?> getUsers() {
        return ResponseEntity.ok(userService.getUsers());
    }

    @DeleteMapping("/{id}")
    public HttpEntity<?> deleteUser(@PathVariable UUID id) {
        ApiResponse response = userService.deleteUser(id);
        return ResponseEntity.status(response.isSuccess() ? HttpStatus.OK : HttpStatus.CONFLICT).body(response);
    }

    @PatchMapping("/edit")
    public HttpEntity<?> editUser(@CurrentUser User onUser, @RequestBody User user) {
        ApiResponse response = userService.editUser(onUser.getId(),user);
        return ResponseEntity.status(response.isSuccess() ? HttpStatus.ACCEPTED : HttpStatus.CONFLICT).body(response);
    }


}
