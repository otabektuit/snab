package uz.obdev.appsnab.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import uz.obdev.appsnab.entity.Order;
import uz.obdev.appsnab.entity.User;
import uz.obdev.appsnab.payload.ApiResponse;
import uz.obdev.appsnab.payload.ReqOrder;
import uz.obdev.appsnab.repository.OrderRepository;
import uz.obdev.appsnab.security.CurrentUser;
import uz.obdev.appsnab.service.OrderService;
import uz.obdev.appsnab.utils.AppConstants;

import java.util.UUID;

@RestController
@RequestMapping("/api/order")
public class OrderController {
    @Autowired
    OrderService orderService;
    @Autowired
    OrderRepository orderRepository;

    @PostMapping("/add")
    public HttpEntity<?> add(@RequestBody ReqOrder reqOrder, @CurrentUser User user) {
        ApiResponse response = orderService.addOrEditOrder(reqOrder, null);
        return ResponseEntity.status(response.isSuccess() ? HttpStatus.CREATED : HttpStatus.CONFLICT).body(response);
    }

    @GetMapping("/getOrderByUser")
    public HttpEntity<?> getOrders(@RequestParam(value = "page", defaultValue = AppConstants.DEFAULT_PAGE) int page,
                                      @RequestParam(value = "size", defaultValue = AppConstants.DEFAULT_SIZE) int size, @CurrentUser User user) {
        return ResponseEntity.ok(orderService.getOrdersByUser(page, size, user.getId()));
    }

    @PutMapping("/edit/{id}")
    public HttpEntity<?> edit(@PathVariable UUID id,@RequestBody ReqOrder reqOrder, @CurrentUser User user) {
        ApiResponse response = orderService.addOrEditOrder(reqOrder, id);
        return ResponseEntity.status(response.isSuccess() ? HttpStatus.ACCEPTED : HttpStatus.CONFLICT).body(response);
    }

    @GetMapping("/{id}")
    public HttpEntity<?> getOrder(@PathVariable UUID id){
        Order order=orderRepository.findById(id).orElseThrow(()->new ResourceNotFoundException("getOrder"));
        return ResponseEntity.ok(orderService.getOrder(order));
    }

    @PreAuthorize(value = "hasRole('ROLE_ADMIN')")
    @GetMapping()
    public HttpEntity<?> getOrderByAdmin(@RequestParam(value = "page", defaultValue = AppConstants.DEFAULT_PAGE) int page,
                                             @RequestParam(value = "size", defaultValue = AppConstants.DEFAULT_SIZE) int size) {
        return ResponseEntity.ok(orderService.getOrders(page,size));
    }

    @DeleteMapping("/deleteOrder/{id}")
    public HttpEntity<?> delete(@PathVariable UUID id){
        ApiResponse response=orderService.deleteOrder(id);
        return ResponseEntity.status(response.isSuccess()?HttpStatus.OK:HttpStatus.CONFLICT).body(response);
    }

}
