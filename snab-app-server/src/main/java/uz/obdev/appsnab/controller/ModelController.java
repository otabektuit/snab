package uz.obdev.appsnab.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uz.obdev.appsnab.entity.Model;
import uz.obdev.appsnab.payload.ApiResponse;
import uz.obdev.appsnab.payload.ReqModel;
import uz.obdev.appsnab.repository.ModelRepository;
import uz.obdev.appsnab.service.ModelService;
import uz.obdev.appsnab.utils.AppConstants;

import java.util.UUID;


@RestController
@RequestMapping("/api/model")
public class ModelController {
    @Autowired
    ModelService modelService;
    @Autowired
    ModelRepository modelRepository;

    @GetMapping
    public HttpEntity<?> getModels(@RequestParam(value = "page", defaultValue = AppConstants.DEFAULT_PAGE) int page,
                                   @RequestParam(value = "size", defaultValue = AppConstants.DEFAULT_SIZE) int size) {
        return ResponseEntity.ok(modelService.getModels(page, size));
    }

    @GetMapping("/{id}")
    public HttpEntity<?> getModel(@PathVariable UUID id) {
        Model model = modelRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("getModel"));
        return ResponseEntity.ok(modelService.getModel(model));
    }

    @PostMapping
    public HttpEntity<?> addModel(@RequestBody ReqModel reqModel) {
        ApiResponse response = modelService.addModel(reqModel);
        return ResponseEntity.status(response.isSuccess() ? HttpStatus.CREATED : HttpStatus.CONFLICT).body(response);
    }

    @PutMapping("/{id}")
    public HttpEntity<?> editModel(@PathVariable UUID id, @RequestBody ReqModel reqModel) {
        ApiResponse response = modelService.edit(id, reqModel);
        return ResponseEntity.status(response.isSuccess() ? HttpStatus.ACCEPTED : HttpStatus.CONFLICT).body(response);
    }

    @DeleteMapping("/{id}")
    public HttpEntity<?> deleteModel(@PathVariable UUID id) {
        ApiResponse response = modelService.delete(id);
        return ResponseEntity.status(response.isSuccess() ? HttpStatus.OK : HttpStatus.CONFLICT).body(response);
    }

    @GetMapping("/list")
    public HttpEntity<?> getModelOpen() {
        ApiResponse response = modelService.getModelsOpen();
        return ResponseEntity.status(response.isSuccess() ? HttpStatus.OK : HttpStatus.CONFLICT).body(response);
    }

    @GetMapping("/filterByProduct")
    public HttpEntity<?> filterByProduct(@ModelAttribute(value = "productId") Integer id) {
        return ResponseEntity.ok(modelService.filterByProduct(id));
    }

    @GetMapping("/filterByBrand")
    public HttpEntity<?> filterByBrand(@ModelAttribute(value = "brandId") UUID id) {
        return ResponseEntity.ok(modelService.filterByBrand(id));
    }


    @GetMapping("/findModelByName")
    public HttpEntity<?> findModelByName(@RequestParam(value = "modelName") String value) {
        return ResponseEntity.ok(modelService.findModelByName(value));
    }
}
