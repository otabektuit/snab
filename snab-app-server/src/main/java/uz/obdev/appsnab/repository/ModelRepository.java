package uz.obdev.appsnab.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.obdev.appsnab.entity.Brand;
import uz.obdev.appsnab.entity.Model;
import uz.obdev.appsnab.entity.Product;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface ModelRepository extends JpaRepository<Model, UUID> {
    List<Model> findAllByProductId(Integer id);
    List<Model> findAllByBrandId(UUID id);
    List<Model> findAllByNameContaining(String name);
}
