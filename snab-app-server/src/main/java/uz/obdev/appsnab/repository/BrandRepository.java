package uz.obdev.appsnab.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;
import uz.obdev.appsnab.entity.Brand;
import uz.obdev.appsnab.entity.Product;
import uz.obdev.appsnab.projection.CustomBrand;

import java.util.List;
import java.util.UUID;

@RepositoryRestResource(path = "/brand",excerptProjection = CustomBrand.class)
public interface BrandRepository extends JpaRepository<Brand, UUID> {
//    @RestResource(path = "/filterByProduct")
//    List<Brand> findAllByProductId(@Param("id") UUID id);
}
