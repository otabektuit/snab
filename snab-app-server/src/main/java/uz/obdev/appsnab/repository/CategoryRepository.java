package uz.obdev.appsnab.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;
import uz.obdev.appsnab.entity.Category;
import uz.obdev.appsnab.projection.CustomCategory;

import java.util.List;

@RepositoryRestResource(path = "/category",excerptProjection = CustomCategory.class)
public interface CategoryRepository extends JpaRepository<Category,Integer> {
    @RestResource(path = "/serial")
    @Query(value = "select * from product",nativeQuery= true)
    List<Category> getSerialCategory();
}
