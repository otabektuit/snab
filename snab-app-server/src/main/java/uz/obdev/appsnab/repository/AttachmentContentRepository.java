package uz.obdev.appsnab.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.obdev.appsnab.entity.Attachment;
import uz.obdev.appsnab.entity.AttachmentContent;

import java.util.Optional;
import java.util.UUID;

public interface AttachmentContentRepository extends JpaRepository<AttachmentContent, UUID> {
    Optional<AttachmentContent> findByAttachment(Attachment attachment);
    void deleteByAttachmentId(UUID attachment_id);
}
