package uz.obdev.appsnab.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;
import uz.obdev.appsnab.entity.Product;
import uz.obdev.appsnab.projection.CustomProduct;

import java.util.List;

@RepositoryRestResource(path = "/product",excerptProjection = CustomProduct.class)
public interface ProductRepository extends JpaRepository<Product, Integer> {
    @RestResource(path = "/filterByCategory")
    List<Product> findAllByCategoryId(@Param("id") Integer id);
}
