package uz.obdev.appsnab.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import uz.obdev.appsnab.entity.Order;

import java.util.UUID;


public interface OrderRepository extends JpaRepository<Order, UUID> {
    Page<Order> findAllByCreatedBy(UUID createdBy, Pageable pageable);
}
