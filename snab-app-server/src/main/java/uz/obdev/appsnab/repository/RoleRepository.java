package uz.obdev.appsnab.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.obdev.appsnab.entity.Role;
import uz.obdev.appsnab.entity.enums.RoleName;

public interface RoleRepository extends JpaRepository<Role, Integer> {
    Role findByRoleName(RoleName roleUser);
}
