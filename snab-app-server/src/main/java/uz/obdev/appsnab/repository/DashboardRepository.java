package uz.obdev.appsnab.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.obdev.appsnab.entity.Dashboard;

import java.util.UUID;

public interface DashboardRepository extends JpaRepository<Dashboard, UUID> {
}
