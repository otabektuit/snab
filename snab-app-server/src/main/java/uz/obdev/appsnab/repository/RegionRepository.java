package uz.obdev.appsnab.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;
import uz.obdev.appsnab.entity.Region;
import uz.obdev.appsnab.projection.CustomRegion;

import java.util.List;

@RepositoryRestResource(path = "/region", excerptProjection = CustomRegion.class)
public interface RegionRepository extends JpaRepository<Region, Integer> {

    @RestResource(path = "/filterByCountry")
    List<Region> findAllByCountryId(@Param("id") Integer id);
}
