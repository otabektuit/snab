package uz.obdev.appsnab.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.obdev.appsnab.entity.AttachmentType;
import uz.obdev.appsnab.entity.enums.AttachmentTypeEnum;

import java.util.UUID;

public interface AttachmentTypeRepository extends JpaRepository<AttachmentType, UUID> {
    AttachmentType findByAttachmentTypeEnum(AttachmentTypeEnum attachmentTypeEnum);
}