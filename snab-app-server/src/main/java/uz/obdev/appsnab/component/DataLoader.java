package uz.obdev.appsnab.component;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import uz.obdev.appsnab.entity.User;
import uz.obdev.appsnab.repository.RoleRepository;
import uz.obdev.appsnab.repository.UserRepository;

@Component
public class DataLoader implements CommandLineRunner {
    @Value("${spring.datasource.initialization-mode}")
    private String initialMode;
    @Autowired
    UserRepository userRepository;

    @Autowired
    PasswordEncoder passwordEncoder;
    @Autowired
    RoleRepository roleRepository;

    @Override
    public void run(String... args) throws Exception {
        if (initialMode.equals("always")) {
            userRepository.save(
                    new User(
                            "Salohiddin",
                            "Salohiddin",
                            "+998977506361",
                            passwordEncoder.encode("root123"),
                            roleRepository.findAll(),
                            "saloh224@gmail.com"
                    )
            );
        }
    }
}
