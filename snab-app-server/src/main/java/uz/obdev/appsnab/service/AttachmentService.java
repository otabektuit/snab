package uz.obdev.appsnab.service;

import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import uz.obdev.appsnab.entity.Attachment;
import uz.obdev.appsnab.entity.AttachmentContent;
import uz.obdev.appsnab.payload.ApiResponse;
import uz.obdev.appsnab.repository.AttachmentContentRepository;
import uz.obdev.appsnab.repository.AttachmentRepository;
import uz.obdev.appsnab.repository.AttachmentTypeRepository;

import java.io.IOException;
import java.util.Iterator;
import java.util.Optional;
import java.util.UUID;

@Service
public class AttachmentService {
    private final AttachmentRepository attachmentRepository;
    private final AttachmentContentRepository attachmentContentRepository;
    private final AttachmentTypeRepository attachmentTypeRepository;

    public AttachmentService(AttachmentRepository attachmentRepository, AttachmentContentRepository attachmentContentRepository, AttachmentTypeRepository attachmentTypeRepository) {
        this.attachmentRepository = attachmentRepository;
        this.attachmentContentRepository = attachmentContentRepository;
        this.attachmentTypeRepository = attachmentTypeRepository;
    }

    public ApiResponse saveFile(MultipartHttpServletRequest request) throws IOException {
        Iterator<String> fileNames = request.getFileNames();
        MultipartFile file = request.getFile(fileNames.next());
//        if (!request.getParameter("type").equals("undefined")) {
        // AttachmentType attachmentType = attachmentTypeRepository.findByAttachmentTypeEnum(AttachmentTypeEnum.valueOf(request.getParameter("type")));
//            if (!attachmentType.getContentTypes().contains(file.getContentType())) {
//                return new ApiResponse("Faylning tipi " + attachmentType.getContentTypes() + " laridan biri bo'lishi shart", false);
//            }
//            if (file.getContentType().contains("image")) {
//                BufferedImage image = ImageIO.read(file.getInputStream());
//                if (!(image.getWidth() <= attachmentType.getWidth() && image.getHeight() <= attachmentType.getHeight())) {
//                    return new ApiResponse("Rasmning o'lchovi " + attachmentType.getWidth() + "x" + attachmentType.getHeight() + " bo'lishi shart", false);
//                }
//            }
//            if (file.getSize() > attachmentType.getSize()) {
//                return new ApiResponse("Faylning hajmi " + attachmentType.getSize() + " dan oshmasligi shart", false);
//            }
        Attachment attachment = new Attachment(
                file.getOriginalFilename(),
                file.getContentType(),
                file.getSize()
        );
        Attachment savedAttachment = attachmentRepository.save(attachment);
        try {
            AttachmentContent attachmentContent = new AttachmentContent(file.getBytes(), savedAttachment);
            attachmentContentRepository.save(attachmentContent);
            return new ApiResponse("Saqlandi", true, savedAttachment.getId());
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
        //}
//        return new ApiResponse("Mavjud bo'lmagan type tanlangan", false);
    }

    public HttpEntity<?> getFile(UUID id) {
        Attachment attachment = attachmentRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("getAttachment"));
        Optional<AttachmentContent> attachmentContent = attachmentContentRepository.findByAttachment(attachment);
        return ResponseEntity.ok().contentType(MediaType.valueOf(attachment.getContentType()))
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + attachment.getName() + "\"")
                .body(attachmentContent.get().getBytes());
    }

    public ApiResponse deleteFile(UUID id) {
        if (attachmentRepository.findById(id).isPresent()) {
            Optional<AttachmentContent> attachmentContent = attachmentContentRepository.findByAttachment(attachmentRepository.findById(id).get());
            if (attachmentContent.isPresent()) {
                attachmentContentRepository.delete(attachmentContent.get());
                attachmentRepository.deleteById(id);
                return new ApiResponse("Rasm ochirildi", true);
            }
        }
        return new ApiResponse("Bunday rasm mavjud emas", false);
    }
}
