package uz.obdev.appsnab.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import uz.obdev.appsnab.entity.User;
import uz.obdev.appsnab.payload.ApiResponse;
import uz.obdev.appsnab.payload.ResUser;
import uz.obdev.appsnab.repository.UserRepository;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class UserService {
    final
    UserRepository userRepository;
    final
    PasswordEncoder passwordEncoder;

    public UserService(UserRepository userRepository, PasswordEncoder passwordEncoder) {
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
    }

    public ResUser getUser(User user) {
        return new ResUser(
                user.getId(),
                user.getFirstName(),
                user.getLastName(),
                user.getPhoneNumber(),
                user.getEmail(),
                user.getRoles(),
                user.getCreatedAt()
        );
    }

    public ApiResponse getUsers() {
        List<User> users = userRepository.findAll();
        List<ResUser> resUsers = users.stream().map(this::getUser).collect(Collectors.toList());
        return new ApiResponse("Mana users", true, resUsers);
    }

    public ApiResponse deleteUser(UUID id) {
        if (userRepository.existsById(id)) {
            userRepository.deleteById(id);
            return new ApiResponse("User o'chdi", true);
        }
        return new ApiResponse("User mavjud emas", false);
    }

    public ApiResponse editUser(UUID id,User user) {
        User newUser = userRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("getUser"));
        if (newUser != null) {
            if (user.getFirstName() != null && !user.getFirstName().equals("")) {
                newUser.setFirstName(user.getFirstName());
            }
            if (user.getLastName() != null && !user.getLastName().equals("")) {
                newUser.setLastName(user.getLastName());
            }
            if (user.getEmail() != null && !user.getEmail().equals("")) {
                newUser.setEmail(user.getEmail());
            }
            if (user.getPassword() != null && !user.getPassword().equals("")) {
                newUser.setPassword(passwordEncoder.encode(user.getPassword()));
            }
            userRepository.save(newUser);
            return new ApiResponse("User o'zgardi", true);
        }
        return new ApiResponse("User o'zgarmadi", false);
    }
}
