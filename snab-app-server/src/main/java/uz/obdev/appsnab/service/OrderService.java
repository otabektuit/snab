package uz.obdev.appsnab.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.stereotype.Service;
import uz.obdev.appsnab.entity.Model;
import uz.obdev.appsnab.entity.Order;
import uz.obdev.appsnab.payload.ApiResponse;
import uz.obdev.appsnab.payload.ReqOrder;
import uz.obdev.appsnab.payload.ResOrder;
import uz.obdev.appsnab.payload.ResPageable;
import uz.obdev.appsnab.repository.*;
import uz.obdev.appsnab.utils.CommonUtils;

import java.util.UUID;

@Service
public class OrderService {
    @Autowired
    private OrderRepository orderRepository;
    @Autowired
    private BrandRepository brandRepository;
    @Autowired
    private ModelRepository modelRepository;
    @Autowired
    private ProductRepository productRepository;
    @Autowired
    private UserRepository userRepository;

    public ApiResponse addOrEditOrder(ReqOrder reqOrder, UUID id) {
        try {
            Order order = id == null ? new Order() : orderRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("getOrder"));
            order.setCount(reqOrder.getCount());
            Model model = modelRepository.findById(reqOrder.getModelId()).orElseThrow(() -> new ResourceNotFoundException("getModel"));
            order.setModel(model);
            orderRepository.save(order);
            if (id == null) {
                return new ApiResponse("Zakaz qabul qilindi", true);
            } else {
                return new ApiResponse("Zakaz ozgartirildi", true);
            }
        } catch (Exception e) {
            return new ApiResponse("Zakaz amalida  xatolik yuz berdi", false);
        }
    }

    public ResOrder getOrder(Order order) {
        return new ResOrder(
                order.getId(),
                order.getModel().getProduct().getNameUz(),
                order.getModel().getProduct().getNameRu(),
                order.getModel().getProduct().getNameEn(),
                order.getModel().getName(),
                order.getModel().getBrand().getName(),
                userRepository.findById(order.getCreatedBy()).orElseThrow(()-> new ResourceNotFoundException("getUser")),
                order.getCount(),
                order.getModel().getPrice(),
                order.getCreatedAt()
        );
    }

    public ResPageable getOrdersByUser(int page, int size, UUID userId) {
        Pageable pageable = CommonUtils.getPageable(page, size);
        Page<Order> orderPage = orderRepository.findAllByCreatedBy(userId, pageable);
        return new ResPageable(
                orderPage.getContent().stream().map(this::getOrder),
                page,
                orderPage.getTotalPages(),
                orderPage.getTotalElements()
        );
    }

    public ResPageable getOrders(int page, int size) {
        Pageable pageable = CommonUtils.getPageable(page, size);
        Page<Order> orderPage = orderRepository.findAll(pageable);
        return new ResPageable(
                orderPage.getContent().stream().map(this::getOrder),
                page,
                orderPage.getTotalPages(),
                orderPage.getTotalElements()
        );
    }

    public ApiResponse deleteOrder(UUID id) {
        if (orderRepository.findById(id).isPresent()) {
            orderRepository.deleteById(id);
            return new ApiResponse("Zakaz bekor qilindi", true);
        }
        return new ApiResponse("Bunday zakaz mavjud emas", false);
    }

}
