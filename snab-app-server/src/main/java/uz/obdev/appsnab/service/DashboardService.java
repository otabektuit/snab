package uz.obdev.appsnab.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.stereotype.Service;
import uz.obdev.appsnab.entity.Dashboard;
import uz.obdev.appsnab.payload.ApiResponse;
import uz.obdev.appsnab.payload.ReqDashboard;
import uz.obdev.appsnab.payload.ResDashboard;
import uz.obdev.appsnab.repository.AttachmentContentRepository;
import uz.obdev.appsnab.repository.AttachmentRepository;
import uz.obdev.appsnab.repository.DashboardRepository;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class DashboardService {

    private final DashboardRepository dashboardRepository;
    final
    AttachmentRepository attachmentRepository;
    @Autowired
    AttachmentContentRepository attachmentContentRepository;

    public DashboardService(DashboardRepository dashboardRepository, AttachmentRepository attachmentRepository) {
        this.dashboardRepository = dashboardRepository;
        this.attachmentRepository = attachmentRepository;
    }

    public ApiResponse addDashboard(ReqDashboard reqDashboard) {
        try {
            Dashboard dashboard = new Dashboard();
            dashboard.setNameUz(reqDashboard.getNameUz());
            dashboard.setNameRu(reqDashboard.getNameRu());
            dashboard.setNameEn(reqDashboard.getNameEn());
            dashboard.setDescrpUz(reqDashboard.getDescrpUz());
            dashboard.setDescrpRu(reqDashboard.getDescrpRu());
            dashboard.setDescrpEn(reqDashboard.getDescrpEn());
            dashboard.setAboutUz(reqDashboard.getAboutUz());
            dashboard.setAboutRu(reqDashboard.getAboutRu());
            dashboard.setAboutEn(reqDashboard.getAboutEn());
            dashboard.setAttachment(attachmentRepository.findById(reqDashboard.getAttachmentId()).orElseThrow(() -> new ResourceNotFoundException("get Attachment")));
            dashboardRepository.save(dashboard);
            return new ApiResponse("Dashboard saqlandi", true);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new ApiResponse("Dashboard saqlanmadi!", false);
    }

    public ApiResponse deleteDashboard(UUID id) {
        if (dashboardRepository.findById(id).isPresent()) {
            dashboardRepository.deleteById(id);
            return new ApiResponse("Dashboard o'chirildi", true);
        }
        return new ApiResponse("Dashboard o'chirilmadi!", false);
    }

    public ReqDashboard getDashboard(Dashboard dashboard) {
        return new ReqDashboard(
                dashboard.getId(),
                dashboard.getAttachment().getId(),
                dashboard.getNameUz(),
                dashboard.getNameRu(),
                dashboard.getNameEn(),
                dashboard.getAboutUz(),
                dashboard.getAboutRu(),
                dashboard.getAboutEn(),
                dashboard.getDescrpUz(),
                dashboard.getDescrpRu(),
                dashboard.getDescrpEn()
        );

    }

    public ResDashboard getResDashboard(Dashboard dashboard) {
        return new ResDashboard(
                dashboard.getId(),
                dashboard.getAttachment().getId(),
                dashboard.getNameUz(),
                dashboard.getNameRu(),
                dashboard.getNameEn(),
                dashboard.getAboutUz(),
                dashboard.getAboutRu(),
                dashboard.getAboutEn(),
                dashboard.getDescrpUz(),
                dashboard.getDescrpRu(),
                dashboard.getDescrpEn(),
                dashboard.getCreatedAt()
        );

    }


    public List<ResDashboard> getDashboards() {
        return dashboardRepository.findAll().stream().map(this::getResDashboard).collect(Collectors.toList());
    }


    public ApiResponse editDashboard(UUID id, ReqDashboard reqDashboard) {
        try {
            Dashboard dashboard = dashboardRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("getDashboard"));
            dashboard.setAboutEn(reqDashboard.getAboutEn());
            dashboard.setAboutRu(reqDashboard.getAboutRu());
            dashboard.setAboutUz(reqDashboard.getAboutUz());
            dashboard.setAttachment(attachmentRepository.findById(reqDashboard.getAttachmentId()).orElseThrow(() -> new ResourceNotFoundException("getAttachment")));
            dashboard.setDescrpEn(reqDashboard.getDescrpEn());
            dashboard.setDescrpRu(reqDashboard.getDescrpRu());
            dashboard.setDescrpUz(reqDashboard.getDescrpUz());
            dashboard.setNameUz(reqDashboard.getNameUz());
            dashboard.setNameRu(reqDashboard.getNameRu());
            dashboard.setNameEn(reqDashboard.getNameEn());
            dashboardRepository.save(dashboard);
            return new ApiResponse("Dashboard ozgartirildi", true);
        } catch (Exception e) {
            return new ApiResponse("Dashborda ozgarishda xatolik", false);
        }
    }
}
