package uz.obdev.appsnab.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.stereotype.Service;
import uz.obdev.appsnab.entity.Model;
import uz.obdev.appsnab.entity.template.AbsEntity;
import uz.obdev.appsnab.payload.ApiResponse;
import uz.obdev.appsnab.payload.ReqModel;
import uz.obdev.appsnab.payload.ResModel;
import uz.obdev.appsnab.payload.ResPageable;
import uz.obdev.appsnab.repository.AttachmentRepository;
import uz.obdev.appsnab.repository.BrandRepository;
import uz.obdev.appsnab.repository.ModelRepository;
import uz.obdev.appsnab.repository.ProductRepository;
import uz.obdev.appsnab.utils.CommonUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class ModelService {
    final
    AttachmentRepository attachmentRepository;
    final
    ProductRepository productRepository;
    final
    BrandRepository brandRepository;
    final
    ModelRepository modelRepository;

    public ModelService(AttachmentRepository attachmentRepository, ProductRepository productRepository, BrandRepository brandRepository, ModelRepository modelRepository) {
        this.attachmentRepository = attachmentRepository;
        this.productRepository = productRepository;
        this.brandRepository = brandRepository;
        this.modelRepository = modelRepository;
    }

    public ApiResponse addModel(ReqModel reqModel) {
        try {
            Model model = new Model();
            model.setName(reqModel.getName());
            model.setBrand(brandRepository.findById(reqModel.getBrandId()).orElseThrow(() -> new ResourceNotFoundException("getBrand")));
            model.setProduct(productRepository.findById(reqModel.getProductId()).orElseThrow(() -> new ResourceNotFoundException("getProduct")));
            model.setAttachments(attachmentRepository.findAllById(reqModel.getPhotosId()));
            model.setOption(reqModel.getOption());
            model.setPrice(reqModel.getPrice());
            modelRepository.save(model);
            return new ApiResponse("Model saqlandi!", true);
        } catch (Exception e) {
            return new ApiResponse("Model saqlanmadi!", false);
        }
    }

    public ResModel getModel(Model model) {
        return new ResModel(
                model.getId(),
                model.getName(),
                model.getAttachments().stream().map(AbsEntity::getId).collect(Collectors.toList()),
                model.getBrand().getName(),
                model.getBrand().getId(),
                model.getProduct().getNameUz(),
                model.getProduct().getNameRu(),
                model.getProduct().getNameEn(),
                model.getProduct().getId(),
                model.getOption(),
                model.getPrice()
        );
    }

    public ResPageable getModels(int page, int size) {
        Pageable pageable = CommonUtils.getPageable(page, size);
        Page<Model> companyPage = modelRepository.findAll(pageable);
        return new ResPageable(
                companyPage.getContent().stream().map(this::getModel).collect(Collectors.toList()),
                page,
                companyPage.getTotalPages(),
                companyPage.getTotalElements());
    }

    public ApiResponse edit(UUID id, ReqModel reqModel) {
        Model model = modelRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("getModel"));
        if (model != null) {
            try {
                model.setName(reqModel.getName());
                model.setOption(reqModel.getOption());
                model.setPrice(reqModel.getPrice());
                model.setBrand(brandRepository.findById(reqModel.getBrandId()).orElseThrow(() -> new ResourceNotFoundException("getBrand")));
                model.setAttachments(attachmentRepository.findAllById(reqModel.getPhotosId()));
                model.setOption(reqModel.getOption());
                model.setPrice(reqModel.getPrice());
                modelRepository.save(model);
                return new ApiResponse("Model o'zgardi", true);
            } catch (Exception e) {
                return new ApiResponse("Model o'zgarmadi", false);
            }
        }
        return new ApiResponse("Model topilmadi!", false);
    }

    public ApiResponse delete(UUID id) {
        if (modelRepository.findById(id).isPresent()) {
            modelRepository.deleteById(id);
            return new ApiResponse("Model o'chdi", true);
        }
        return new ApiResponse("Model mavjud emas!", false);
    }

    public ApiResponse getModelsOpen() {
        List<ResModel> models = modelRepository.findAll().stream().map(this::getModel).collect(Collectors.toList());
        List<ResModel> modelsOpen = new ArrayList<>();
        for (int i = models.size() - 1; i > -1; i--) {
            modelsOpen.add(models.get(i));
        }
        if (modelsOpen != null) {
            return new ApiResponse("Mana models", true, modelsOpen);
        }
        return new ApiResponse("Models not exists", false);
    }

    public List<ResModel> filterByProduct(Integer id) {
        List<Model> models=modelRepository.findAllByProductId(id);
        return models.stream().map(this::getModel).collect(Collectors.toList());
    }

    public List<ResModel> filterByBrand(UUID id){
        List<Model> models=modelRepository.findAllByBrandId(id);
        return models.stream().map(this::getModel).collect(Collectors.toList());
    }

    public List<ResModel> findModelByName(String value){
        List<Model> models=modelRepository.findAllByNameContaining(value);
        return models.stream().map(this::getModel).collect(Collectors.toList());
    }
}
