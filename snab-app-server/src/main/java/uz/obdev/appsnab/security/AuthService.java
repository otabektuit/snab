package uz.obdev.appsnab.security;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthException;
import com.google.firebase.auth.UserRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import uz.obdev.appsnab.entity.User;
import uz.obdev.appsnab.entity.enums.RoleName;
import uz.obdev.appsnab.payload.ApiResponse;
import uz.obdev.appsnab.payload.ReqUser;
import uz.obdev.appsnab.repository.RoleRepository;
import uz.obdev.appsnab.repository.UserRepository;

import java.util.Collections;
import java.util.UUID;

@Service
public class AuthService implements UserDetailsService {
    @Autowired
    UserRepository userRepository;
    @Autowired
    RoleRepository roleRepository;
    @Autowired
    PasswordEncoder passwordEncoder;

    public ApiResponse addUser(ReqUser reqUser) {
        if (!userRepository.existsByPhoneNumber(reqUser.getPhoneNumber())) {
            User user = new User();
            String activeCode = UUID.randomUUID().toString();
            user.setFirstName(reqUser.getFirstName());
            user.setLastName(reqUser.getLastName());
            user.setPhoneNumber(reqUser.getPhoneNumber());
            user.setPassword(passwordEncoder.encode(reqUser.getPassword()));
            if (!userRepository.existsByEmail(reqUser.getEmail())) {
                user.setEmail(reqUser.getEmail());
            } else {
                return new ApiResponse("Bunday email li user sistemada mavjud", false);
            }
            user.setRoles(Collections.singletonList(roleRepository.findByRoleName(RoleName.ROLE_USER)));
            userRepository.save(user);
            return new ApiResponse("User ro'yxatdan o'tkazildi", true);
        }
        return new ApiResponse("Bunday telefon raqamli user sistemada mavjud", false);
    }

    public Boolean getCheckUserSignUp(String phoneNumber) {
        try {
            UserRecord userRecord = FirebaseAuth.getInstance().getUserByPhoneNumber(phoneNumber);
            if (userRecord != null)
                return !userRepository.existsByPhoneNumber(phoneNumber);
            return false;
        } catch (FirebaseAuthException e) {
            e.printStackTrace();
            return false;
        }
    }

    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
        return userRepository.findByPhoneNumber(s).orElseThrow(() -> new UsernameNotFoundException("User id no validate"));
    }

    public UserDetails loadUserByUserId(UUID id) {
        return userRepository.findById(id).orElseThrow(() -> new UsernameNotFoundException("User id no validate"));
    }
}
