package uz.obdev.appsnab.payload;

import lombok.Data;

@Data
public class JwtResponse {
    private String tokeType = "Tusiq";
    private String tokenBody;
    public JwtResponse(String tokenBody) {
        this.tokenBody = tokenBody;
    }
}
