package uz.obdev.appsnab.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.UUID;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ResModel {
    private UUID id;
    private String name;
    private List<UUID> photosId;
    private String brandName;
    private UUID brandId;
    private String productNameUz;
    private String productNameRu;
    private String productNameEn;
    private Integer productId;
    private String option;
    private double price;
}
