package uz.obdev.appsnab.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class ReqUser {
    private String phoneNumber;
    private String password;
    private String firstName;
    private String lastName;
    private String email;
}
