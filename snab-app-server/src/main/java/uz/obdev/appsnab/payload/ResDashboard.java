package uz.obdev.appsnab.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Timestamp;
import java.util.UUID;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ResDashboard {
    private UUID id;
    private UUID attachmentId;
    private String nameUz;
    private String nameRu;
    private String nameEn;
    private String aboutUz;
    private String aboutRu;
    private String aboutEn;
    private String descrpUz;
    private String descrpRu;
    private String descrpEn;
    private Timestamp createdAt;
}
