package uz.obdev.appsnab.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.UUID;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ReqModel {
    private UUID id;
    private String name;
    private UUID brandId;
    private Integer productId;
    private List<UUID> photosId;
    private String option;
    private double price;
}
