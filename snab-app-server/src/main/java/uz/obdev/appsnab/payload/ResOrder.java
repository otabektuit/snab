package uz.obdev.appsnab.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import uz.obdev.appsnab.entity.User;

import java.sql.Timestamp;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ResOrder {
    private UUID id;
    private String productNameUz;
    private String productNameRu;
    private String productNameEn;
    private String modelName;
    private String brandName;
    private User user;
    private int count;
    private Double price;
    private Timestamp createdAt;
}
