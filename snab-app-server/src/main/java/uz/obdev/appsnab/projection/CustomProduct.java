package uz.obdev.appsnab.projection;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.rest.core.config.Projection;
import uz.obdev.appsnab.entity.Product;


@Projection(name = "customProduct", types = Product.class)
public interface CustomProduct {
    Integer getId();

    String getNameUz();

    String getNameRu();

    String getNameEn();


    @Value(value = "#{target.category.id}")
    Integer getCategoryId();

    @Value(value = "#{target.category.nameUz}")
    String getCategoryNameUz();

    @Value(value = "#{target.category.nameRu}")
    String getCategoryNameRu();

    @Value(value = "#{target.category.nameEn}")
    String getCategoryNameEn();
}
