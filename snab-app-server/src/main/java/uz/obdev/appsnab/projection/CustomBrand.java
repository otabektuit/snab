package uz.obdev.appsnab.projection;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.rest.core.config.Projection;
import uz.obdev.appsnab.entity.Brand;
import uz.obdev.appsnab.entity.Product;

import java.util.UUID;

@Projection(name = "customBrand", types = Brand.class)
public interface CustomBrand {
    UUID getId();

    String getName();

//    @Value(value = "#{target.product.id}")
//    Integer getProductId();
//
//    @Value(value = "#{target.product.nameUz}")
//    String getProductNameUz();
//
//    @Value(value = "#{target.product.nameRu}")
//    String getProductNameRu();
//
//    @Value(value = "#{target.product.nameEn}")
//    String getProductNameEn();
}
