package uz.obdev.appsnab.projection;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.rest.core.config.Projection;

import javax.swing.plaf.synth.Region;

@Projection(name = "customRegion", types = Region.class)
public interface CustomRegion {
    Integer getId();

    String getNameUz();

    String getNameRu();

    String getNameEn();

    @Value(value = "#{target.country.id}")
    Integer getCountryId();

    @Value(value = "#{target.country.nameUz}")
    String getCountryNameUz();

    @Value(value = "#{target.country.nameRu}")
    String getCountryNameRu();

    @Value(value = "#{target.country.nameEn}")
    String getCountryNameEn();

}
