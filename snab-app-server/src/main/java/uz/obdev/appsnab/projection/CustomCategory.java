package uz.obdev.appsnab.projection;

import org.springframework.data.rest.core.config.Projection;
import uz.obdev.appsnab.entity.Category;

@Projection(name = "customCategory", types = Category.class)
public interface CustomCategory {
    Integer getId();

    String getNameUz();

    String getNameRu();

    String getNameEn();
}
